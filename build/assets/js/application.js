(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function(document) {
  var toggle = document.querySelector('.sidebar-toggle');
  var sidebar = document.querySelector('#sidebar');
  var checkbox = document.querySelector('#sidebar-checkbox');

  document.addEventListener('click', function(e) {
    var target = e.target;

    if(!checkbox.checked ||
      sidebar.contains(target) ||
      (target === checkbox || target === toggle)) return;

    checkbox.checked = false;
  }, false);
})(document);

/**
 * Google Analytics
 *
 * @type {Array}
 * @private
 */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '{{ site.analytics }}']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

},{}],2:[function(require,module,exports){
'use strict';

var resizeElement = function(widthOrHeight) {
  var wordSearch = $('#wordsearch');
  var wordSearchLI = wordSearch.find('li');
  wordSearchLI.css('height', widthOrHeight);
  wordSearchLI.css('lineHeight', widthOrHeight);
  var totalHeight = wordSearch.css('width');
  wordSearch.css('height', totalHeight);
}

var toWords = function toWords(n) {
  if (n == 0) return 'zero';
  var a = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
  var b = ['', '', 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
  var g = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion'];
  var grp = function grp(n) {
    return ('000' + n).substr(-3);
  };
  var rem = function rem(n) {
    return n.substr(0, n.length - 3);
  };
  var fmt = function fmt(_ref) {
    var h = _ref[0];
    var t = _ref[1];
    var o = _ref[2];

    return [Number(h) === 0 ? '' : a[h] + ' hundred ', Number(o) === 0 ? b[t] : b[t] && b[t] + '-' || '', a[t + o] || a[o]].join('');
  };
  var cons = function cons(xs) {
    return function (x) {
      return function (g) {
        return x ? [x, g && ' ' + g || '', ' ', xs].join('') : xs;
      };
    };
  };
  var iter = function iter(str) {
    return function (i) {
      return function (x) {
        return function (r) {
          if (x === '000' && r.length === 0) return str;
          return iter(cons(str)(fmt(x))(g[i]))(i + 1)(grp(r))(rem(r));
        };
      };
    };
  };
  return iter('')(0)(grp(String(n)))(rem(String(n)));
};

/*
 *
 */
var notifyMe = function(httpStatusCode) {
  var messageValue = httpStatusCode;

  function responseSuccess(response, data) {
    console.log(response);
    console.log(data);
  }

  function responseError(data) {
    var mailMessage = {};
    mailMessage.name = 'GRGRT-DE';
    mailMessage.email = 'steve@graegert.de';
    mailMessage.message = data;
    $.ajax({
      url: '/php/mailer.php',
      type: 'POST',
      dataType: 'json',
      data: mailMessage
    });
  }

  var message = [
    {name: 'text', value: messageValue},
    {name: 'username', value: 'GRGRT-DE'}
  ];
  
  message.parse = 'full';
  $.ajax({
    url: '/php/slack/send-40x-report.php',
    type: 'POST',
    dataType: 'json',
    data: message,
    error: function (xhr, status, error) {
      console.log(['grgrt.error', status, error, xhr.responseText]);
    },
    success: function (response) {
      if (response.success.length !== 0 && response.success === true) {
        responseSuccess(response, message);
      } else {
        responseError(message);
      }
    }
  });
}

},{}]},{},[1,2])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvX2Fzc2V0cy9qcy9tYWluLmpzIiwic3JjL19hc3NldHMvanMvc2NyaXB0cy00MHguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIoZnVuY3Rpb24oZG9jdW1lbnQpIHtcbiAgdmFyIHRvZ2dsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zaWRlYmFyLXRvZ2dsZScpO1xuICB2YXIgc2lkZWJhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzaWRlYmFyJyk7XG4gIHZhciBjaGVja2JveCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzaWRlYmFyLWNoZWNrYm94Jyk7XG5cbiAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgdmFyIHRhcmdldCA9IGUudGFyZ2V0O1xuXG4gICAgaWYoIWNoZWNrYm94LmNoZWNrZWQgfHxcbiAgICAgIHNpZGViYXIuY29udGFpbnModGFyZ2V0KSB8fFxuICAgICAgKHRhcmdldCA9PT0gY2hlY2tib3ggfHwgdGFyZ2V0ID09PSB0b2dnbGUpKSByZXR1cm47XG5cbiAgICBjaGVja2JveC5jaGVja2VkID0gZmFsc2U7XG4gIH0sIGZhbHNlKTtcbn0pKGRvY3VtZW50KTtcblxuLyoqXG4gKiBHb29nbGUgQW5hbHl0aWNzXG4gKlxuICogQHR5cGUge0FycmF5fVxuICogQHByaXZhdGVcbiAqL1xudmFyIF9nYXEgPSBfZ2FxIHx8IFtdO1xuX2dhcS5wdXNoKFsnX3NldEFjY291bnQnLCAne3sgc2l0ZS5hbmFseXRpY3MgfX0nXSk7XG5fZ2FxLnB1c2goWydfdHJhY2tQYWdldmlldyddKTtcblxuKGZ1bmN0aW9uKCkge1xuICB2YXIgZ2EgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTsgZ2EudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnOyBnYS5hc3luYyA9IHRydWU7XG4gIGdhLnNyYyA9ICgnaHR0cHM6JyA9PSBkb2N1bWVudC5sb2NhdGlvbi5wcm90b2NvbCA/ICdodHRwczovL3NzbCcgOiAnaHR0cDovL3d3dycpICsgJy5nb29nbGUtYW5hbHl0aWNzLmNvbS9nYS5qcyc7XG4gIHZhciBzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpWzBdOyBzLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGdhLCBzKTtcbn0pKCk7XG4iLCIndXNlIHN0cmljdCc7XG5cbnZhciByZXNpemVFbGVtZW50ID0gZnVuY3Rpb24od2lkdGhPckhlaWdodCkge1xuICB2YXIgd29yZFNlYXJjaCA9ICQoJyN3b3Jkc2VhcmNoJyk7XG4gIHZhciB3b3JkU2VhcmNoTEkgPSB3b3JkU2VhcmNoLmZpbmQoJ2xpJyk7XG4gIHdvcmRTZWFyY2hMSS5jc3MoJ2hlaWdodCcsIHdpZHRoT3JIZWlnaHQpO1xuICB3b3JkU2VhcmNoTEkuY3NzKCdsaW5lSGVpZ2h0Jywgd2lkdGhPckhlaWdodCk7XG4gIHZhciB0b3RhbEhlaWdodCA9IHdvcmRTZWFyY2guY3NzKCd3aWR0aCcpO1xuICB3b3JkU2VhcmNoLmNzcygnaGVpZ2h0JywgdG90YWxIZWlnaHQpO1xufVxuXG52YXIgdG9Xb3JkcyA9IGZ1bmN0aW9uIHRvV29yZHMobikge1xuICBpZiAobiA9PSAwKSByZXR1cm4gJ3plcm8nO1xuICB2YXIgYSA9IFsnJywgJ29uZScsICd0d28nLCAndGhyZWUnLCAnZm91cicsICdmaXZlJywgJ3NpeCcsICdzZXZlbicsICdlaWdodCcsICduaW5lJywgJ3RlbicsICdlbGV2ZW4nLCAndHdlbHZlJywgJ3RoaXJ0ZWVuJywgJ2ZvdXJ0ZWVuJywgJ2ZpZnRlZW4nLCAnc2l4dGVlbicsICdzZXZlbnRlZW4nLCAnZWlnaHRlZW4nLCAnbmluZXRlZW4nXTtcbiAgdmFyIGIgPSBbJycsICcnLCAndHdlbnR5JywgJ3RoaXJ0eScsICdmb3VydHknLCAnZmlmdHknLCAnc2l4dHknLCAnc2V2ZW50eScsICdlaWdodHknLCAnbmluZXR5J107XG4gIHZhciBnID0gWycnLCAndGhvdXNhbmQnLCAnbWlsbGlvbicsICdiaWxsaW9uJywgJ3RyaWxsaW9uJywgJ3F1YWRyaWxsaW9uJywgJ3F1aW50aWxsaW9uJywgJ3NleHRpbGxpb24nLCAnc2VwdGlsbGlvbicsICdvY3RpbGxpb24nLCAnbm9uaWxsaW9uJ107XG4gIHZhciBncnAgPSBmdW5jdGlvbiBncnAobikge1xuICAgIHJldHVybiAoJzAwMCcgKyBuKS5zdWJzdHIoLTMpO1xuICB9O1xuICB2YXIgcmVtID0gZnVuY3Rpb24gcmVtKG4pIHtcbiAgICByZXR1cm4gbi5zdWJzdHIoMCwgbi5sZW5ndGggLSAzKTtcbiAgfTtcbiAgdmFyIGZtdCA9IGZ1bmN0aW9uIGZtdChfcmVmKSB7XG4gICAgdmFyIGggPSBfcmVmWzBdO1xuICAgIHZhciB0ID0gX3JlZlsxXTtcbiAgICB2YXIgbyA9IF9yZWZbMl07XG5cbiAgICByZXR1cm4gW051bWJlcihoKSA9PT0gMCA/ICcnIDogYVtoXSArICcgaHVuZHJlZCAnLCBOdW1iZXIobykgPT09IDAgPyBiW3RdIDogYlt0XSAmJiBiW3RdICsgJy0nIHx8ICcnLCBhW3QgKyBvXSB8fCBhW29dXS5qb2luKCcnKTtcbiAgfTtcbiAgdmFyIGNvbnMgPSBmdW5jdGlvbiBjb25zKHhzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh4KSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGcpIHtcbiAgICAgICAgcmV0dXJuIHggPyBbeCwgZyAmJiAnICcgKyBnIHx8ICcnLCAnICcsIHhzXS5qb2luKCcnKSA6IHhzO1xuICAgICAgfTtcbiAgICB9O1xuICB9O1xuICB2YXIgaXRlciA9IGZ1bmN0aW9uIGl0ZXIoc3RyKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChpKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKHgpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgaWYgKHggPT09ICcwMDAnICYmIHIubGVuZ3RoID09PSAwKSByZXR1cm4gc3RyO1xuICAgICAgICAgIHJldHVybiBpdGVyKGNvbnMoc3RyKShmbXQoeCkpKGdbaV0pKShpICsgMSkoZ3JwKHIpKShyZW0ocikpO1xuICAgICAgICB9O1xuICAgICAgfTtcbiAgICB9O1xuICB9O1xuICByZXR1cm4gaXRlcignJykoMCkoZ3JwKFN0cmluZyhuKSkpKHJlbShTdHJpbmcobikpKTtcbn07XG5cbi8qXG4gKlxuICovXG52YXIgbm90aWZ5TWUgPSBmdW5jdGlvbihodHRwU3RhdHVzQ29kZSkge1xuICB2YXIgbWVzc2FnZVZhbHVlID0gaHR0cFN0YXR1c0NvZGU7XG5cbiAgZnVuY3Rpb24gcmVzcG9uc2VTdWNjZXNzKHJlc3BvbnNlLCBkYXRhKSB7XG4gICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICB9XG5cbiAgZnVuY3Rpb24gcmVzcG9uc2VFcnJvcihkYXRhKSB7XG4gICAgdmFyIG1haWxNZXNzYWdlID0ge307XG4gICAgbWFpbE1lc3NhZ2UubmFtZSA9ICdHUkdSVC1ERSc7XG4gICAgbWFpbE1lc3NhZ2UuZW1haWwgPSAnc3RldmVAZ3JhZWdlcnQuZGUnO1xuICAgIG1haWxNZXNzYWdlLm1lc3NhZ2UgPSBkYXRhO1xuICAgICQuYWpheCh7XG4gICAgICB1cmw6ICcvcGhwL21haWxlci5waHAnLFxuICAgICAgdHlwZTogJ1BPU1QnLFxuICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgIGRhdGE6IG1haWxNZXNzYWdlXG4gICAgfSk7XG4gIH1cblxuICB2YXIgbWVzc2FnZSA9IFtcbiAgICB7bmFtZTogJ3RleHQnLCB2YWx1ZTogbWVzc2FnZVZhbHVlfSxcbiAgICB7bmFtZTogJ3VzZXJuYW1lJywgdmFsdWU6ICdHUkdSVC1ERSd9XG4gIF07XG4gIFxuICBtZXNzYWdlLnBhcnNlID0gJ2Z1bGwnO1xuICAkLmFqYXgoe1xuICAgIHVybDogJy9waHAvc2xhY2svc2VuZC00MHgtcmVwb3J0LnBocCcsXG4gICAgdHlwZTogJ1BPU1QnLFxuICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgZGF0YTogbWVzc2FnZSxcbiAgICBlcnJvcjogZnVuY3Rpb24gKHhociwgc3RhdHVzLCBlcnJvcikge1xuICAgICAgY29uc29sZS5sb2coWydncmdydC5lcnJvcicsIHN0YXR1cywgZXJyb3IsIHhoci5yZXNwb25zZVRleHRdKTtcbiAgICB9LFxuICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgaWYgKHJlc3BvbnNlLnN1Y2Nlc3MubGVuZ3RoICE9PSAwICYmIHJlc3BvbnNlLnN1Y2Nlc3MgPT09IHRydWUpIHtcbiAgICAgICAgcmVzcG9uc2VTdWNjZXNzKHJlc3BvbnNlLCBtZXNzYWdlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc3BvbnNlRXJyb3IobWVzc2FnZSk7XG4gICAgICB9XG4gICAgfVxuICB9KTtcbn1cbiJdfQ==
