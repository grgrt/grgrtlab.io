---
title: Slack Everything - Sending Form Data to a Slack channel
layout: post
date: 2016-01-31
description: Re-invent form data submission with Slack.
---

Until recently I used email to receive data from online forms.  A plain and simple POST request to a specific URL to load a small PHP script which invoked `email()`. Simple and convenient, but not always practical.  Sharing the contents of an Email within changing audiences is rarely straightforward.  

Sending form data to [Slack](https://slack.com), for instance, simplifies sharing that information to specific target groups, usually represented by *channels*, significantly, because the message can be pushed to many users at once, overcoming a lot of weaknesses of email when it comes to sharing.

For example, to forward your contact form data, you'd have to know all recipients personally and have their email addresses.  Also, with email only, new team members will have a hard time to get up to date, because Email has no concept of history: all emails sent to this date are potentially lost for new colleagues.


## Example

The following example illustrates how to submit form data to Slack (contact form data, for instance).  A JavaScript performs data collection and error handling, and a simple PHP snippet will talk to Slack.

```html
<body class="theme-base-08">
  ...
</body>
```

You are not required to use PHP at all, but since JavaScript is handled by the browser it will expose the channel ID in plain text allowing unauthorised (malicious?) users to spam the channel without doing any serious damage, except for being a nuisance for regular users.

A server-side component (e.g. PHP or whatever floats your boat) provides sufficient protection.  You will understand shortly how nicely both worlds can play together. 


### Contact Form

```html
<form action="/php/mailer.php" id="contact-form" method="post">
  <div>
    <div class="two fields">
      <div class="field">
        <input name="name" placeholder="Name" type="text">
      </div>
      <div class="field">
        <input name="email" placeholder="Email Address" type="text">
      </div>
    </div>
    <div class="one field">
      <textarea name="message" placeholder="Message" id="message">
      </textarea>
    </div>
    <div class="field">
      <div class="submit button">Submit</div>
    </div>
  </div>
  <div class="error message"></div>
</form>
```

Validation messages will be displayed in the bottom `<div>` which is invisible on page init.


### Submitting to Slack

The following script is responsible for wiring up the submit event with an event handler and for processing AJAX responses returned from calling the PHP script.

The Immediately-Invoked Function Expression (IIFE) is activated when the document has been loaded 

sfdsds

```javascript
(function() {
  var errorClass = 'error';
  var successClass = 'success';

  /**
   * Is invoked on successful calls to the mail form handler.
   * @param {Object} form - The form that was used to submit the message.
   * @param {string} data - String to be returned to users.
   */
  function mailerResponseSuccess(form, data) {
    var responseHolder = form.find('.ui.message');
    var inputs = form.find('input[type=text], input[type=email], input[type=password], textarea');
    var message = data || 'Thank you.  Your email was sent successfully!';
    responseHolder.addClass('success').html(message);
    inputs.removeClass(errorClass).addClass(successClass);
  }

  /**
   * Called if message could not be sent to Slack.
   * @param {Object} form - The form that was used to submit the message.
   * @param {Object|string} data - String to be returned to users; object containing error information.
   */
  function mailerResponseError(form, data) {
    var responseHolder = form.find('.ui.message');
    var inputs = form.find('input[type=text], input[type=email], input[type=password], textarea');
    var message = (typeof data === 'string') ? data : 'Sorry, AJAX error occurred';
    // If data not a scalar string
    if (typeof data === 'object') {
      var a = [];
      $.each(data, function(key, value) {
        a.push(value);
      });
      message = a.join('. ');
    }

    responseHolder.addClass(errorClass).html(message);
    inputs.addClass(errorClass);
  }

  // Mailer form handler
  $(document).on('submit', '#contact-form', function(e) {
    const form = $(this);
    var formData = form.serializeArray();
    // Prevent form from submitting, we have plans
    e.preventDefault();
    formData.parse = 'full';
    $.ajax({
      url: '/php/slack/send-message.php',
      type: 'POST',
      dataType: 'json',
      data: formData,
      error: function(xhr, status, error) {
        console.log(['grgrt.slack.error', status, error, xhr.responseText]);
      },
      success: function(response) {
        if (response.success.length !== 0 && response.success === true) {
          mailerResponseSuccess(form, 'Thank you.  Your email was sent successfully!');
        } else {
          mailerResponseError(form, response.data);
        }
      }
    });
  });
})();
```

