---
layout: post
title: 'SharePoint: Error Accessing Search Settings in SSP'
date: 2009-03-22
description: 'Accessing the Search Service administration site for the first time might result in an error since there may be no SSL certificate issued for the host configured with the query/index/search role.  This post illustrates how to fix the problem.'
---


### The Issue

Last week I've setup a completely new virtual MOSS 2007 farm for our lab.  While most part of the setup went well I was unable to load the *Search Settings page* in the *Shared Services Administration* site.
{: .post-intro}

![SSP Search Settings link resulting in an exception when followed](/assets/img/posts/sspsearchsettingslink.png)

The ULS wrote the following error message:

```cs
Exception caught in Search Admin web-service proxy (client). System.Net.WebException:
  The underlying connection was closed: An unexpected error occurred on a send.
  ---> System.IO.IOException: Authentication failed because the remote party has closed the transport stream.
  at System.Net.Security.SslState.StartReadFrame(Byte[] buffer, Int32 readBytes, AsyncProtocolRequest asyncRequest)
  at System.Net.Security.SslState.StartReceiveBlob(Byte[] buffer, AsyncProtocolRequest asyncRequest)
  at System.Net.Security.SslState.CheckCompletionBeforeNextReceive(ProtocolToken message, AsyncProtocolRequest asyncRequest)
  at System.Net.Security.SslState.StartSendBlob(Byte[] incoming, Int32 count, AsyncProtocolRequest asyncRequest)
  at System.Net.Security.SslState.ForceAuthentication(Boolean receiveFirst, Byte[] buffer, AsyncProtocolRequest asyncRequest)
  at System.Net.Security.SslState.ProcessAuthentication(LazyAsyncResult lazyResult)
  at System.Net.TlsStream.CallProcessAuthentication(Object state)
  at System.Threading.ExecutionContext.runTryCode(Object userData)
  at System.Runtime.CompilerServices.RuntimeHelpers.ExecuteCodeWithGuaranteedCleanup(TryCode code, CleanupCode backoutCode, Object userData)
  at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state)
  at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state)
  at System.Net.TlsStream.ProcessAuthentication(LazyAsyncResult result)
  at System.Net.TlsStream.Write(Byte[] buffer, Int32 offset, Int32 size)
  at System.Net.PooledStream.Write(Byte[] buffer, Int32 offset, Int32 size)
  at System.Net.ConnectStream.WriteHeaders(Boolean async)
```


The farm consisted of the following hosts and roles

  * 1 dedicated Central Administration host
  * 2 Web Frontends used for content presentation exclusively
  * 1 application server hosting hosting the SSP and MySites
  * 1 Query and indexing server (no dedicated crawl host)

The error was returned by the query server that has been contacted by the Central Administration host via SSL on the default port 56738.


### The Solution

Inspecting the stack trace revealed that there seems to be an issue with the SSL certificate at the query server's Office Server Web Services website.  Since I have not issued a certificate for that web site and I seriously doubt SharePoint has done it on its own during setup I decided to run selfssl.exe in order to fix the problem.

  1. `selfssl.exe` is part of the __IIS 6.0 Resource Kit Tools__. Download from [Microsoft.com](http://www.microsoft.com/downloads/details.aspx?FamilyID=56fc92ee-a71a-4c73-b628-ade629c89499&DisplayLang=en).
  2. Install the RKT on the target server.  In my case: the query and index host
  3. Select _Start > IIS Resources > SelfSSL > SelfSSL_ from the Start Menu
  4. Enter the following command:

    `selfssl.exe /N:CN=[hostname] /K:1024 /V:365 /S:[ID] /P:56738`

Arguments:


*	`/N` is the hostname the certificate will be associated with
*	`/K` specifies the key size, this parameter is optional, key size is 1024 bytes by default
*	`/V` represents the certificate's validity in days.
*	`/S` must match the Office Server Web Services website's ID (use the IIS Manager to get it)
*	`/P` specifies the port number for SSL transactions (SharePoint default is 56738)


This should fix the problem.  Please, let me know if it worked for you.

Thanks for reading.
