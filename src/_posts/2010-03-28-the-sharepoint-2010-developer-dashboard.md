---
title: The SharePoint 2010 Developer Dashboard
layout: post
date: 2010-03-28
description: "The Developer Dashboard available with SharePoint 2010 is a great tool for developers to get valuable insights about internal processes."
---

By default the Dashboard is disabled. In this post I will introduce three methods to activate the Developer Dashboard and illustrate its usage with a simple example.
{: .post-intro}

## Using `STSADM`

Open a console and run the following `stsadm` command:

```
stsadm -o setproperty -pn developer-dashboard -pv ondemand
```

Running this command may take a while. The `ondemand` property value instructs SharePoint to make the Dashboard available but to allow users to turn it on and off on-demand. Alternatively you can provide the values `on` to activate the Dashboard permanently and `off` to deactivate it.

## Using a PowerShell Script


While `stsadm` is sufficient for ad-hoc scenarios you may want to look at a PowerShell script for a more flexible and, most importantly, scriptable way to enable the SharePoint Developer Dashboard. The following script sets the Dashboard's display level to the value specified in `$setting` and prints the result after modifying the SharePoint configuration.

``` cs
# Possible values for $setting = {On, Off, OnDemand}
$setting = "OnDemand"
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint")
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Administration")
$contentSvc = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
Write-Host("Current setting: " + $contentSvc.DeveloperDashboardSettings.DisplayLevel)
$contentSvc.DeveloperDashboardSettings.DisplayLevel = ([Enum]::Parse(
    [Microsoft.SharePoint.Administration.SPDeveloperDashboardLevel], $setting)
)
$contentSvc.DeveloperDashboardSettings.Update()
Write-Host("New setting: " + $contentSvc.DeveloperDashboardSettings.DisplayLevel)

trap [System.ArgumentException] {
    Write-Host "The provided display level '$setting' is invalid."
    continue;
}
```

![Enabling the dashboard via PowerShell](/assets/img/posts/2010-03-28-devdas01.png)


## Using Code


As the provided PowerShell script suggests there exists a managed API for performing the same task using code. This approach is pretty straight forward:

```cs
SPWebService s = SPWebService.ContentService;
s.DeveloperDashboardSettings.DisplayLevel = SPDeveloperDashboardLevel.OnDemand;
s.DeveloperDashboardSettings.Update();
```

## Making Controls and Web Parts Dashboard-aware


It is best practice to make your controls and web parts Dashboard-aware. This is accomplished by employing the `SPMonitoredScope` class demonstrated in the following example:

```cs
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.Utilities;

namespace DeveloperDashboard {
    public class DashboardAwareControl : UserControl {
        protected void Page_Load(object sender, EventArgs e) {
            using (new SPMonitoredScope(GetType().Name)) {
                var button = new Button { Text = "Don't klick me!" };
                Controls.Add(button);

                using (new SPMonitoredScope("Processing operation")) {
                    for (var i = 0; i < 100000000; i++) {
                        Math.Atan(i / Math.Sqrt(i));
                    }
                }
            }
        }
    }
}
```

Nesting `SPMonitoredScope` instances allows for fine-grained tracing of internal operations. As you can see the inner scope dubbed _Processing operation_ is displayed as a node within the parent scope.

![Output captured by the dashboard](/assets/img/posts/2010-03-28-devdas02.png)

Worth mentioning is the fact that sandboxed component are not captured by the Developer Dashboard because sandboxed components execute in a different process than that the page request originated from.

As always, thanks for reading.
