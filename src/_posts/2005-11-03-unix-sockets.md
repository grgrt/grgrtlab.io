---
title: "UNIX Socket Programming Made Easy"
layout: post
date: 2005-11-03
description: 'Get familiar with basic concepts of socket programming on UNIX focusing on TCP and UDP'

---

You should have a good grasp of the C programming language and know your way around in your favorite UNIX.  Fundamentals about the TCP/IP protocol stack is definitely helpful.
{: .post-intro}


## Contents

* TOC
{:toc}


### Concepts

The socket API has been developed for the Berkeley Software Distribution (BSD) and has since widely acknowledged as the state-of-the-art multipurpose network API and employed by effectively every UNIX system. When doing networking under UNIX it's very likely that communication is done via _sockets_.

A socket is uniquely identified by its associated IP address and port number. This way it's possible to open several connections using the same IP address but different port numbers, each socket serving a completely independent connection.

First of all, sockets must be established (properly setup) before data can be sent to a remote or local host. It has a name, which is called a _socket descriptor_ and an integer used to identify a specific socket among API calls.

A socket is also established on the endpoint of your communication, either by explicitly doing so, which is the case for clients, or implicitly when a server is listening for incoming connections. A socket is associated with the following characteristics:

* **Address family:** identifies what kind of address of a protocol is to be used for this socket
* **Socket type:** indicates how data is transferred, e.g. as a record based sequence of bytes or as a byte stream, etc.
* **Protocol family:** specifies what kind of protocol is used, e.g. IP, ISO, etc.


### System Calls and Data Structures


All three previously outlined parameters must be supplied as arguments to the `socket` system call which returns a socket descriptor or -1 on error:

```c
#include <sys/socket.h>

int socket(int family, int type, int protocol);
```

`family` can be one of the following constants:

| **Constant** | **Address family** |
|--------------|--------------------|
| `AF_UNIX` | Unix Domain Sockets |
| `AF_INET`, `AF_INET6` | Internet Protocol Version 4 and 6 |
| `AF_APPLETALK` | Appletalk Protocol |

There are lots of protocol families not listed here but you can find an almost complete list in `socket.h`. `type` is a constant indicating the socket type:

* `SOCK_STREAM` - Provides sequenced, reliable, two-way, connection-based byte streams. An out-of-band data transmission mechanism may be supported.
* `SOCK_DGRAM` - Supports datagrams (connectionless, unreliable messages of a fixed maximum length).
* `SOCK_SEQPACKET` - Provides a sequenced, reliable, two-way connection-based data transmission path for datagrams of fixed maximum length; a consumer is required to read an entire packet with each read system call.
* `SOCK_RAW` - Provides raw network protocol access; useful for protocol testing.
* `SOCK_RDM` - Provides a reliable datagram layer that does not guarantee ordering.
* `SOCK_PACKET` - Obsolete and should not be used in new programs.

The most important ones are `SOCK_STREAM` for TCP and `SOCK_DGRAM` for UDP. Finally there is `protocol` that specifies which protocol to use for the specified address family and socket type. If only one protocol is available you can safely set it to 0, otherwise use one of the constants below (defined in `socket.h`:

* `IPPROTO_TCP` - Transmission Control Protocol
* `IPPROTO_UDP` - User Datagram Protocol
* `IPPROTO_IP` - For use with Internet Protocol suite (TCP/IP)
* `IPPROTO_RAW` - For raw socket communication

Be aware that you cannot mix values for `family`, `type` and `protocol` arbitrarily. They must match. For example you cannot specify `AF_UNIX` and then give `SOCK_DGRAM`. Writing a server and a client both involves calling certain functions of the BSD socket API. An important difference is the calling sequence for client and server applications.

**Calling sequence for server applications:**

* `socket()` - create a suitable socket and obtain socket descriptor
* `bind()` - assign socket a name (actually not a name but an IP and a port)
* `listen()` - listen for incoming connections to serve
* `accept()` - serve incoming connections from connection queue


**Calling sequence for client applications:**

* `socket()`: create a suitable socket and obtain socket descriptor
* `connect()`: connect to remote endpoint

As we can see, writing a server applications involves a couple of function calls whereas clients only acquire a socket and call `connect()`. To open a socket for TCP communication, call `socket()` as follows:

```c
int sockfd = socket(AF_INET, SOCK_STREAM, 0);

if (sockfd == -1) {
    perror("socket()"); /* handle error */
    exit(-1);
}
```

The last argument indicates that the default protocol family for `AF_INET` should be used. It's equivalent to `IPPROTO_TCP` Now we have created a socket, but cannot use it for communication, yet. We must first _bind_ it calling `bind()`:

```c
#include <sys/socket.h>

int bind(int sockfd, struct sockaddr *addr, socklen_t addrlen);
```

The first argument is the socket descriptor of the socket we want to bind, secondly we provide a valid `sockaddr` structure (described shortly), and provide the length of the structure. The `sockaddr` structure is a generic structure large enough to hold the largest of all address structures (yes, there are more address structures to come). Its primary purpose is to ease conversion of different structures, since at the time of the API design ANSI C was not existent and a `void` pointer was not known which would have served the purpose. It is defined as follows:

```c
struct sockaddr {
    unsigned short sa_family; /* address family */
    char sa_data[14];         /* 14 bytes of protocol address */
}
```

Most of the time you'll have to deal with either `sockaddr_in` for IP or `sockaddr_in6` for IPv6. Both are similar an defined as follows:

```c
struct sockaddr_in {
    sa_family_t    sin_family; /* address family: AF_INET */
    u_int16_t      sin_port;   /* port in network byte order */
    struct in_addr sin_addr;   /* internet address */
};

struct sockaddr_in6 {
    sa_family_t     sin6_family;    /* AF_INET6 */
    in_port_t       sin6_port;      /* Port number. */
    uint32_t        sin6_flowinfo;  /* traffic class, flow info. */
    struct in6_addr sin6_addr;      /* IPv6 address. */
    uint32_t        sin6_scope_id;  /* Interfaces for a scope. */
}
```

A very confusing point about address structures is the embedded `in_addr` and `in6_addr` which holds the IP address in network byte order, so you don't have to convert between byte orders as shown below.

The origin of this construct is unknown to the author (please email me or leave a comment if you have details). Before calling `bind` and passing the structure you will have to set certain fields. Usually you'll first clear all fields and then set them appropriately:

```c
sockaddr_in server;
memset((char *)&server, 0, sizeof(sockaddr_in));
server.sin_family = AF_INET;
server.sin_port = htons(PORT);
server.sin_addr.s_addr = INADDR_ANY;
```

Since byte order may be different on host systems and on networks, it has been agreed upon a default byte order for data that is going to be transferred between endpoints: network byte order (NBO).

Accordingly, host byte order (HBO) is the byte order used on the local host and can be either big endian or little endian. All data that is passed through the protocol stack must be converted to NBO as we have seen with the `port` member of `sockaddr_in`.  Header `netinet/in.h` provides for some useful function to convert between HBO and NBO:

```c
#include <arpa/inet.h>

uint32_t htonl(uint32_t hostlong);
uint16_t htons(uint16_t hostshort);
uint32_t ntohl(uint32_t netlong);
uint16_t ntohs(uint16_t netshort);
```

At first glance these function names are cryptic but translate as follows: `h` is for `host`, `n` for network and `l` and `s` for `long` (double word, 32 bits) and `short` (single word, 16 bits) respectively. Back to `bind`. The function takes a `socketaddr` structure but for IP we use either `sockaddr_in` or `sockaddr_in6`. We will have to cast the structure when passing it to `bind`:

```c
if (bind(fd, (struct sockaddr *)&server, sizeof(server)) < 0) {
    perror("bind()");
    exit(-1)
}
```
To put the server into listening mode call `listen()`:

```c
#include <unistd.h>

int listen(int sock_descriptor, int backlog);
```

passing the socket descriptor as the first argument and the length of the connection queue as desired. The maximum length of the queue depends on a system configuration value, but it's safe to pass a value between 5 to 20. When new connection requests arrive they are put into a queue of _incomplete connections_. To serve a connection we have to call accept that fetches the first incomplete connection.

```c
#include <sys/socket.h>

int accept(int fd, struct sockaddr *addr, socklen_t *addrlen);
```

First argument is, as usual, the socket descriptor. The next two arguments are filled in by the kernel, all you need to do is send the appropriate data. It is worth noting that `accept()` returns a new socket descriptor that refers to a _completed connection_. The descriptor of the incomplete connection is no longer needed and can be closed:

```c
#include <unistd.h>

int close(int fd);</unistd.h>
```

When writing a client we do not call `bind()`, `listen()` or `accept()`, but instead `connect()`:

```c
#include <unistd.h>

int connect(int fd, const struct sockaddr *address, socklen_t addrlen);
```

with `fd` as the socket descriptor, `address` pointing to a `sockaddr` structure containing the peer address and `addrlen` specifying the length of address.


### Writing a TCP Client and Server


In the preceding section we have seen which functions to call in order to set up a client and a server. Let's get our hands dirty and write a small client. It is supposed to send simple commands to a server that returns a response.

**Client code**

```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/param.h>
#include <errno.h>

char buf[BUFSIZ];

main (int argc, char **argv) {
    int sock, n;
    struct sockaddr_in sa;
    struct hostent *host;

    if (argc != 3) {
        printf("Usage: %s <hostname> <port>\n", argv[0]);
        exit (-1);
    }

    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("socket");
        exit(-1);
    }

    if ((host = gethostbyname(argv[1])) == NULL) {
        perror("gethostbyname");
        exit(-1);
    }

    memcpy((char*)&sa.sin_addr, (char*)host->h_addr,
    host->h_length);
    sa.sin_family = AF_INET;
    sa.sin_port = htons((u_short)atoi(argv[2]));

    if (connect(sock, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
        perror("connect");
        exit(-1);
    }

    while (strcmp(buf, "quitn") != 0) {
        while (!((buf[strlen(buf) - 2] == '>') &&
        (buf[strlen(buf) - 1] == '>'))) {
            if ((n = read(sock, buf, sizeof(buf))) < 0) {
                perror("read");
                exit(-1);
            }

            buf[n] = 0;
            printf("%s", buf);
        }

        if (strcmp(buf, "[. in new line terminates]n>>") == 0) {
            while (buf[0]!='.') {
                fgets(buf, sizeof(buf), stdin);
                    if (write(sock, buf, strlen(buf))<0) {
                        perror("write");
                        exit(-1);
                    }
            }
        } else {
            fgets(buf, sizeof(buf), stdin);

            if (write(sock, buf, strlen(buf))<0) {
                perror("write");
                exit(-1);
            }
        }
    }

    return (0);
}
```

**Server code**

```c
#include <netinet/in.h>
#include <socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>

int main (int argc, char **argv) {
    struct sockaddr_in sa;
    struct sockaddr_in caller;
    int ssd, csd, length, retval, action;
    char buf[BUFSIZ];
    char* prompt[] = {
        ">>",
        "Available commands: \n
        \tput - to send a text to server\n
        \thelp - to view this help\n
        \tquit - to close the connection\n>>",
        "Teminate with '.' in a new line.\n>>",
        "Enter a command\n>>"
    };

    if (argc != 2) {
        fprintf(stdout, "usage: %s, \n", argv[0]);
        exit(-1);
    }

    sa.sin_family= AF_INET;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port= htons((u_short)atoi(argv[1]));

    if ((ssd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror ("socket");
        exit(-1);
    }

    if (bind(ssd, (struct sockaddr *)&sa, sizeof sa) < 0) {
        perror ("bind");
        exit(-1);
    }

    length = sizeof(sa);
    listen(ssd, 5);

    if ((csd = accept(ssd, (struct sockaddr *)&caller, &length)) < 0) {
        perror ("accept");
        exut(-1);
    }

    write(csd, prompt[0], strlen(prompt[0]));

    while (action != 2) {
        if ((retval = read(csd, buf, sizeof(buf))) < 0) {
            perror("Reading stream message\n");
            exit(-1);
        }

        buf[retval] = '';
        if (strcmp(buf, "help\n") == 0)
            action = 0;

        if (strcmp(buf, "put\n") == 0)
            action = 1;

        if (strcmp(buf, "quit\n") == 0 || retval==0)
            action = 2;

        if (strcmp(buf, ".\n") == 0)
            action = 3;

        switch (action) {
            case 0:
                write(csd, prompt[1], strlen(prompt[1]));
                action = -1;
                break;
            case 1:
                write(csd, prompt[2], strlen(prompt[2]));
                action = 4;
                break;
            case 2:
                printf("nEnding connection ...\n");
                break;
            case 3:
                write(csd, prompt[3], strlen(prompt[3]));
                action = -1;
                break;
            case 4 :
                printf("%s", buf);
                break;
            default:
                write(csd, prompt[3], strlen(prompt[3]));
        }
    }

    close(csd);
    close(ssd);
    return (0);
}
```

The client works as follows: It connects to the server specified by server name and port at the command line and displays a prompt waiting for input. Entering `help` returns a simple help message:

```
% ./tcpsrv1 9000 &
% ./tcpcli1 localhost 9000
>> help
Available commands:
put - to send a text to server
help - to view this help
quit - to close the connection
Terminate with '.' in a new line.
Enter a command
>> put
Hello server!
Hello server!
>>; quit
Ending connection ...
```

The `put` command simply causes the server to echo its input. `quit` ends the connection. To simplify things we call `gethostbyname()` to obtain the server's IP address. Just take a look at the manpage for `gethostbyname` to find out how it works (it's very easy to use).

Just as seen in the client code `gethostbyname` is utilized in the server code. I previously said that a new descriptor is returned by `accept`. Take a close look at the code and you will see that we pass `ssd` to `accept` and assign the new descriptor to `csd` which is used for communication while connected to the client.


### Improving the Server


Our server has one major drawback: only one client can be served at any given time. We need a way to create another server process for every client requesting a connection. The easiest way to accomplish this is to call `fork()` right after calling `accept`:

```c
while (1) {
	pid_t pid;

    if ((csd = accept(ssd, (struct sockaddr *)&caller, &length)) < 0) {
        perror ("accept");
        exit(-1);
    }
    if ((pid = fork()) == 0) { /* child */
        /* process client */
    } else if (pid < 0) {
        /* fork() failed */
    }

    close(ssd);
}
```

The code for handling the client is done in the `if` branch of `fork` and could be transferred into a separate function to improve readability. When back in parent we close the original socket immediately, since it's not needed anymore, because all communication is handled by the child process.

By enclosing `accept` and `fork` in a while loop that never breaks we are able to server multiple clients. This is possible because `accept` blocks until another connection is ready to be accepted, that is after completing the three-way-handshake.

**The new server code**

```c
#include <sys/types.h>
#include <netinet/in.h>
#include <socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>

char buf[BUFSIZ];
char* prompt[] = {
    ">>",
    "Available commands: \n
    \tput - to send a text to server\n
    \thelp - to view this help\n
    \tquit - to close the connection\n",
    "Teminate with '.' in a new line. >>",
    "Enter a commandn>>"
};

int retval;
int action;

main (int argc, char **argv) {
    struct sockaddr_in sa;
    struct sockaddr_in caller;
    int ssd, csd, length;

    if (argc != 2) {
        fprintf(stdout, "usage: %s\n", argv[0]);
        exit(-1);
    }

    sa.sin_family= AF_INET;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port= htons((u_short)atoi(argv[1]));

    if ((ssd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror ("socket");
        exit(-1);
    }

    if (bind(ssd, (struct sockaddr *)&sa, sizeof sa) < 0) {
        perror ("bind");
        exit(-1);
    }

    length = sizeof(sa);
    listen(ssd, 5);

    while (1) {
        pid_t pid;

        if ((csd = accept(ssd, (struct sockaddr *)&caller, &length)) < 0) {
            perror ("accept");
            exit(-1);
        }

        if ((pid = fork()) == 0) /* child */
            process_client(csd);
        else if (pid < 0)
            perror("fork");

        close(ssd);
    }
}

void process_client(int fd) {
    write(fd, prompt[0], strlen(prompt[0]));

    while (action != 2) {
        if ((retval = read(fd, buf, sizeof(buf))) < 0) {
            perror("Reading stream message\n");
            exit(-1);
        }

        buf[retval] = '';

        if (strcmp(buf, "help\n") == 0)
            action = 0;

        if (strcmp(buf, "put\n") == 0)
            action = 1;

        if (strcmp(buf, "quit\n") == 0 || retval==0)
            action = 2;

        if (strcmp(buf, ".\n") == 0)
            action = 3;

        switch (action) {
            case 0:
                write(fd, prompt[1], strlen(prompt[1]));
                action = -1;
                break;
            case 1:
                write(fd, prompt[2], strlen(prompt[2]));
                action = 4;
                break;
            case 2:
                printf("nEnding connection ...n");
                break;
            case 3:
                write(fd, prompt[3], strlen(prompt[3]));
                action = -1;
                break;
            case 4 :
                printf("%s", buf);
                break;
            default:
                write(fd, prompt[3], strlen(prompt[3]));
        }
    }

    close(ssd);
}
```

Client handling has been put into its own function and the needed variables have been made global (you may want to make these static to limit their scope). Every time a new connection is ready to be accepted a new child process is forked to serve the request. `process_client()` is called by the child and it vanishes when the `quit` command is given thereby breaking the `while` loop closing the socket descriptor of this connection.


### What's next?


Let's make things interesting and find out what it takes to write a multi-threaded server facilitating POSIX Threads (_pthreads_) and how to use the `recv` and `send` system calls instead of `read` and `write`.


## A brief introduction to POSIX Threads


A thread is a stream of instructions that can be scheduled as an independent unit. It is easier to understand a thread within the context of a process. A process is created by an operating system; a process contains information about resources, such as process id, file descriptors, etc.; in addition it contains information pertaining to the execution state, such as program counter and stack. The concept of a thread requires that we make a separation between these two kinds of information in a process:

* resources - these are available to the entire process e.g., program instructions, global data, working directory, etc.
* scheduleable entities - these would include program counters and stacks. A thread is an entity within a process which consists of the scheduleable part of the process.

Each process can have many threads which share the resources within a process (address space, for example). As compared to the cost of creating a process, a thread can be created with much less expense, because there is much less intervention on the part of the operating system.


### What are pthreads?


An unambiguously defined interface is essential if threads are to be useful to programmers that wish to take advantage of the capabilities provided by threads. While hardware vendors each have their own implementation of threads which differ from one another, the emerging standard on Unix systems is specified by IEEE POSIX 1003.1c -1995.

A threads implementation that follows this standard is referred to as _pthreads_. Most hardware vendors now tend to offer pthreads. in addition to their proprietary API's. There are several drafts of the above standard, and most modern UNIX systems and its derivatives such as AIX 4.1+, Solaris 7, Tru64 Unix 5.1+, etc. follows draft 7 and above of the POSIX standard.

It is important to be aware of the draft number of a given implementation, because there are differences between drafts which can cause problems.


### The pthreads API


The API is defined in the ANSI/IEEE POSIX 1003.1 - 1995 standard. As compared to MPI, this standard is not freely available on the Web, and must be purchased from IEEE. An immediate consequence is that a full listing of the pthread functions is only available from the [Single UNIX Specification](http://www.unix.org/single_unix_specification/).


#### Creating and Terminating Threads


Four function calls are involved in managing a thread's lifetime:

```c
#include <pthread.h>

int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
    void *(*start_routine)(void*), void *arg);
void pthread_exit(void *value_ptr);
int pthread_attr_init(pthread_attr_t *attr);
int pthread_attr_destroy(pthread_attr_t *attr);
```

Initially, your `main()` program comprises a single, default thread. All other threads must be explicitly created. `pthread_create` creates a new thread and makes it executable. Typically, threads are first created from within `main()` inside a single process.

Once created, threads are peers, and may create other threads. `pthread_create()` takes the following arguments:

*   `thread` - An opaque, unique identifier for the new thread returned by the subroutine.
*   `attr` - An opaque attribute object that may be used to set thread attributes. You can specify a thread attributes object, or NULL for the default values.
*   `start_routine` - The C routine that the thread will execute once it is created.
*   `arg` - A single argument that may be passed to `start_routine`. It must be passed by reference as a pointer cast of type void. `NULL` may be used if no argument is to be passed.


#### Thread Attributes


By default, a thread is created with certain attributes. Some of these attributes can be changed by the programmer via the thread attribute object. `pthread_attr_init` and `pthread_attr_destroy` are used to initialize/destroy the thread attribute object. Other routines are then used to query/set specific attributes in the thread attribute object.

#### Terminating Threads


There are several ways in which a pthread may be terminated:

*   The thread returns from its starting routine (the main routine for the initial thread)
*   The thread makes a call to the `pthread_exit` subroutine (covered below)
*   The thread is canceled by another thread via the `pthread_cancel` routine (not covered here)
*   The entire process is terminated due to a call to either the `exec` or `exit` subroutines

`pthread_exit` is used to explicitly exit a thread. Typically, the `pthread_exit()` routine is called after a thread has completed its work and is no longer required to exist. If `main()` finishes before the threads it has created, and exits with `pthread_exit()`, the other threads will continue to execute. Otherwise, they will be automatically terminated when `main()` finishes.

You may optionally specify a termination status, which is stored as a void pointer for any thread that may join the calling thread. Please note that the `pthread_exit()` routine does not close files; any files opened inside the thread will remain open after the thread is terminated.


#### Example: Creating and terminating threads


This simple example code creates 10 threads with the `pthread_create()` routine. Each thread prints a _Hello World!_ message, and then terminates with a call to `pthread_exit()`.

```c
#include <pthread.h>
#include <stdlib.h>

#define NUM_THREADS 10

void *print_hello(void *tid) {
    printf("\nThis is thread %d: Hello World!\n", tid);
    pthread_exit(NULL);
}

int main (int argc, char **argv) {
    pthread_t threads[NUM_THREADS];
    int rc, i;

    for (i = 0; i < NUM_THREADS; i++){
        printf("Creating thread %d\n", i);
        rc = pthread_create(&threads[i], NULL,
            print_hello, (void *)i);

        if (rc) {
            printf("ERROR; pthread_create()  returned: %d\n", rc);
            exit(-1);
        }
    }
    pthread_exit(NULL);
}
```

#### Argument passing with threads


As you may have noticed, we can specify a callback function which is called when the associated thread is created. How do we pass an argument to the callback function? The `pthread_create()` routine permits us to pass one argument to the thread start routine.

For cases where multiple arguments must be passed, this limitation is easily overcome by creating a structure which contains all of the arguments, and then passing a pointer to that structure in the routine. All arguments must be passed by reference and cast to `(void *)`.

```c
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define NUM_THREADS 3

char *messages[NUM_THREADS];

struct data {
    int  tid;
    int  sum;
    char *message;
};

struct data data_array[NUM_THREADS];

void *PrintHello(void *arg) {
    int taskid, sum;
    char *hello_msg;
    struct data *my_data;

    sleep(1);
    my_data = (struct data *)arg;
    taskid = my_data->tid;
    sum = my_data->sum;
    hello_msg = my_data->message;
    printf("Thread %d: %s  Sum=%d\n", taskid, hello_msg, sum);

    pthread_exit(NULL);
}

int main(int argc, char **argv) {
    pthread_t threads[NUM_THREADS];
    int *taskids[NUM_THREADS];
    int rc, t, sum;
    sum = 0;

    messages[0] = "English: Hello World!";
    messages[1] = "French: Bonjour, le monde!";
    messages[2] = "German: Guten Tag, Welt!";

    for (t = 0; t < NUM_THREADS; t++) {
        sum = sum + t;
        data_array[t].tid = t;
        data_array[t].sum = sum;
        data_array[t].message = messages[t];

        printf("Creating thread %d\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) &data_array[t]);

        if (rc) {
            printf("ERROR; pthread_create() returned is %d\n", rc);
            exit(-1);
        }
    }

    pthread_exit(NULL);
}
```

It's quite easy to pass data to callback functions of threads. Just provide the data as a `void` pointer and cast it back in the function. That's it.

#### Joining Threads


Joining a thread is a simple way to notify the worker thread that a thread which has been created on behalf of the worker has finished. To join a thread we call `phread_join()` passing the thread ID of the thread to wait for as its argument:

```c
#include <pthread.h>

int pthread_join(pthread_t thread, void **value_ptr);
```

`thread` identifies the thread we want to wait for and `value_ptr` stores the status of the thread that is returned by `pthread_exit()`.  The main thread creates other threads and is able to wait for them to finish. When controlling thread calls `pthread_join()` it is notified when a particular thread is destroyed. The interaction between both threads is outlined in the figure below:

{% include media-image.html file='articles/unix-sockets/figure1.gif' opacity='1' title='Abbildung 1' caption='Joining threads.' %}


#### Example: Joining threads


```c
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define NUM_THREADS 3

void *worker_func(void *null) {
    int i;
    double result = 0.0;

    for (i = 0; i < 1000000; i++)
        result = result + (double)random();

    printf("Thread result = %e\n", result);
    pthread_exit((void *) 0);
}

int main(int argc, char **argv) {
    pthread_t thread[NUM_THREADS];
    pthread_attr_t attr;
    int rc, t, status;

    pthread_attr_init(&attr);

    /* threads are joinable by default; demo only */
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (t = 0; t < NUM_THREADS; t++) {
        printf("Creating thread %d\n", t);
        rc = pthread_create(&thread[t], &attr, worker_func, NULL);

        if (rc) {
            printf("ERROR; pthread_create() returned %d\n", rc);
            exit(-1);
        }
    }

    /* free attribute and wait for the other threads */
    pthread_attr_destroy(&attr);

    for(t = 0; t < NUM_THREADS; t++) {
        rc = pthread_join(thread[t], (void **)&status);

        if (rc) {
            printf("ERROR pthread_join() returned: %d\n", rc);
            exit(-1);
        }

        printf("Join OK with thread %d status = %d\n", t, status);
    }

    pthread_exit(NULL);
}
```

Although not explicitly mentioned in the text we use `pthread_attr_setdetachstate` to ensure that the thread is joinable. POSIX defines threads to be joinable by default, so this has only demonstrative purposes. The detach state of a thread specifies whether the controlling thread is able to watch its execution or if it is completely depended of its controller. In this case a thread is _detached_. The state, among other attributes, is stored in a variable of the opaque data type `pthread_attr_t` which must be initialized by calling `pthread_attr_init` before used first. As you can see threads deserve a complete article on its own. They can increase a program's complexity significantly, so a fundamental understanding of threads is essential if used in larger applications.


## Multi-threaded server


Let's get back to our sockets. To write a multi-threaded server we will need at least two of the functions described in the last section: `pthread_create()` and `pthread_join()`. The server is structured as follows: `main()` calls `client_wait` setting up the address structures, binding the socket and putting the server in listening mode. Listening mode is actually done in function `client_thread` calling `accept`. The newly returned socket descriptor is passed the thread to serve the client. Just as we have seen in the forking server, fetching a connection from the queue must be performed in its own thread. Here is how it works conceptually:

```c
int main(void) {
    socket();
    client_wait();
        /* setup sockaddr_in */
        bind();
        listen();
        client_thread();
            accept();
            pthread_create(client_cb);
            pthread_join();
            client_cb() /* serve client */
```

What follows is the code needed to write a multithreaded server. It already includes the recv and send system calls I am going to explain right after the code. So, here we go.

```c
#include <socket.h>
#include "header.h"

void client_wait(const int); /* listen for requests */
void client_thread(int);     /* create client thread */
void *client_cb(void *);     /* client_thread callback */

int main(int argc, char* argv[]) {
    int sd;

    if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        err_fatal(errno);

    client_wait(sd); /* never returns */
    return 0;
}

void client_wait(const int sd) {
    struct sockaddr_in sa;

    memset((char *)&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(PORT);
    sa.sin_addr.s_addr = INADDR_ANY;

    if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) == -1)
        err_fatal(errno);

    if ((listen(sd, 20)) == -1)
        err_fatal(errno);

    printf("Listening...\n");
    client_thread(sd);
}

void client_thread(int sd) {
    struct sockaddr_in sa;
    int ad;, th_addr_size;
    pthread_t client_th;

    while (1) {
        if ((ad = accept(sd, (struct sockaddr *)&sa, &th_addr_size)) == -1)
            rr_fatal(errno);

        if (pthread_create(client_th, NULL, client_cb, &ad))
            err_fatal(errno);

        printf("Receiving message...\n");
        pthread_join(client_th, NULL); /* sync */
    }
}

void *client_cb(void *arg_list) {
    int *ad = (int *)arg_list; /* connected socket descriptor */
    char buffer[BUF_SIZE];
    int buffer_len, i;

    if (recv(*ad, buffer, BUF_SIZE, 0) == -1)
        err_fatal(errno);

    printf("Message received from client side: %s\n", buffer);
    buffer_len = strlen(buffer);

    for (i = 0; i < buffer_len; i++)
        buffer[i] = toupper(buffer[i]);

    if (send(*ad, buffer, buffer_len, 0) == -1)
        err_fatal(errno);

    printf("Transmition finished!\n");

    if (close(*ad) == -1)
        err_fatal(errno);
}
```

Our `header.h` defines some macros and declares a function for error handling:

```c
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUF_SIZE 512
#define PORT     9000
#define BUFLEN   512
#define NPACK    10

void err_fatal(const int err) {
    char* msg = strerror(err);
    printf("%s", msg);
    exit(-1);
}
```


## `send` and `recv` system calls


`send()` function initiates the transmission of a message to its peer. The socket must be connected before a message can be sent. This is true when a server calls `send()` on an `accept`ed socket and when a client calls `send()` on a `connect`ed socket. The synopsis is defined as follows:

```c
#include <sys/socket.h>

ssize_t send(int socket, const void *buffer, size_t length, int flags);
```

Here, socket is the `accept`ed or `connect`ed socket of the peer to send the message to. `buffer` contains the message to send and `length` specifies its size. To specify the type of transmission, you can set `flags` to one of the following constants that can be logically OR'ed:

* `MSG_EOR` - Terminates a record (if supported by the protocol).
* `MSG_OOB` - Sends out-of-band data on sockets that support out-of-band communications. The significance and semantics of out-of-band data are protocol-specific.

Usually, for simple writing of messages to a socket you can pass 0 (zero). The `recv()` function reads a message from a `connect`ed socket. That's all. It's defined as follows:

```c
#include <sys/socket.h>

ssize_t recv(int socket, void *buffer, size_t length, int flags);
```

Again, socket is the `connect`ed socket to read from and `buffer` points to a memory location to write the message in which is `length` bytes large. The `flags` parameter has a different meaning here:

* `MSG_PEEK` - Peeks at an incoming message. The data is treated as unread and the next `recv()` or similar function shall still return this data.
* `MSG_OOB` - Requests out-of-band data. The significance and semantics of out-of-band data are protocol-specific.
* `MSG_WAITALL` - On SOCK_STREAM sockets this requests that the function block until the full amount of data can be returned. The function may return the smaller amount of data if the socket is a message-based socket, if a signal is caught, if the connection is terminated, if `MSG_PEEK` was specified, or if an error is pending for the socket.

Simply pass 0 (zero) if you do not know what the above constants mean. **`send` and `recv` or `read` and `write`?** It really does not matter what you use to read or write from and to a socket. With the flag options you have the chance to apply special options to the communications, but unless you don't know how to work with OOB or why and when to peek a message, you will be quite happy with `read` and `write` either.

## Writing a UDP server and client


UDP is a connectionless protocol, that is, it's not guaranteed in any way that the data send to a peer arrives or that a peer is notified of an error. It is up to the application the perform error checking. Basically, a few differences to TCP server and client design are worth to be pointed out:

* A UDP server does only call `socket` and `bind`, but not `listen` and `accept`. It's reasonable because with UDP there nothing like a connection. `listen` and `accept` require a connection to listen at.

* A UDP client calls `socket` but not connect for the same reason. All we need to do is call `socket`, and `bind` if working on server code, and then write to or read from the socket. This is done with the `recvfrom` and `sendto` system calls. `recvfrom` is defined as follows:

```c
#include <sys/socket.h>

ssize_t recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
```

Not surprisingly `socket` is the socket of the peer we want to receive a message from, `buffer` points to a memory location suitable to store the messsage in, `length` specifies the length of the buffer, `flags` indicate the type of transmission, `address` points to a properly configured socket address structure and `address_len` specifies the length of the structure.

The `flags` argument has exactly the same semantics as seen for `recv`. `sendto` is defined as follows:

```c
#include <sys/socket.h>

ssize_t sendto(int socket, const void *message, size_t length,
    int flags, const struct sockaddr *dest_addr,
    socklen_t dest_len);
```

Again `socket` is the socket of the peer the message is to be sent to, `message` points to a memory location containing the message to be sent, `length` specifies the size of the message buffer, `flags` indicates the type of transmission, `dest_addr` points to the properly configured socket address structure of the peer and `dest_len` specifies its length.

Let's put things together and write a client and a server.


### UDP server code


```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/param.h>
#include <errno.h>
#include "header.h"

int main(void) {
    struct sockaddr_in local, peer;
    int s, i, slen = sizeof(peer);
    char buf[BUFLEN];

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) > 0)
        err_fatal(errno);

    memset((char *)&local, sizeof(local), 0);
    local.sin_family = AF_INET;
    local.sin_port = htons(PORT);
    local.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(s, &local, sizeof(local)) < 0)
        err_fatal(errno);

    for (i = 0; i < NPACK; i++) {
        if (recvfrom(s, buf, BUFLEN, 0, &peer, &slen) < 0)
            err_fatal(errno);

        printf("Received packet from %s:%d\nData: %s\n\n",
        inet_ntoa(peer.sin_addr), ntohs(peer.sin_port), buf);
    }

    close(s);
    return 0;
}
```

First, we declare receive buffer and create a socket. `AF_INET` says that it will be an Internet socket. `SOCK_DGRAM` indicates that it will use datagram delivery instead of virtual circuits (another term for connection-oriented delivery mode). `IPPROTO_UDP` specifies use of the UDP protocol (the standard transport layer protocol for datagrams in IP networks).

Generally you can use zero for the last parameter; the kernel will figure out what protocol to use (in this case, it would choose `IPPROTO_UDP` anyway). As with the TCP server, our UDP server will accept datagrams from all interfaces specified by `INADDR_ANY`.

Now we are ready to `bind` the socket to the address we created above. This line tells the system that the socket `s` should be bound to the address in `local`. Note that recvfrom() will set `slen` to the number of bytes actually stored. If you want to play safe, set `slen` to `sizeof(peer)` after each call to `recvfrom()`.

The information about the sender we got from `recvfrom()` is displayed (IP:port), along with the data in the packet. `inet_ntoa()` takes a `struct in_addr` and converts it to a string in dot notation, which is rather useful if you want to display the address in a legible form.

### UDP client code


```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/param.h>
#include <errno.h>
#include "header.h"

int main(int argc, char **argv) {
    struct sockaddr_in peer;
    int s, i, slen = sizeof(peer);
    char buf[BUFLEN];

    if (argc != 2) {
        printf("usage: %s\n", argv[0]);
        exit(0);
    }

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        err_fatal(errno);

    memset((char *)&peer, sizeof(peer), 0);
    peer.sin_family = AF_INET;
    peer.sin_port = htons(PORT);

    if (inet_aton(SRV_IP, &peer.sin_addr) == 0)

        err_fatal(errno);
    for (i = 0; i < NPACK; i++) {
        printf("Sending packet %d\n", i);
        sprintf(buf, "This is packet %d\n", i);

        if (sendto(s, buf, BUFLEN, 0, &peer, slen) < 0)
            err_fatal(errno);
    }

    close(s);
    return 0;
}
```

The call to `inet_aton` makes sure that a valid IP address has been given. It will return an error if the address cannot be converted properly.

Optionally, You may call `bind()` after the call to `socket()`, if you wish to specify which port and interface that should be used for the client socket. However, this is almost never necessary and not really recommended. The system will decide what port and interface to use.

Finally, we send `BUFLEN` bytes from `buf` to `s`, with no `flags`. The receiver is specified in `peer`, which contains `slen` byte.

Use the comments section below for your remarks and suggestions. Thanks for reading.
