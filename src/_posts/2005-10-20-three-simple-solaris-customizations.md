---
title: Three Useful Solaris Customizations
layout: post
date: 2004-06-14
description: 'Some customizations for Solaris you may find useful.'
---

Users firing up Solaris 10 for the first time usually have two choices: CDE or JDS (Java Desktop System).  While CDE lacks certain features and the comfort known from other desktop systems, like a one-click window selection area or a pager, it's still my first choice, because it's fast due to an extremely small memory footprint, quite easy to use and stable.  It is therefore perfectly suited for administrative and development tasks.
{: .post-intro}


## Contents

* TOC
{:toc}


## Overview


This is not an homage to the aged _Common Desktop System_ but since I have done some customizations to my systems you may find useful, I'd like to share it with you.  In this article I am going to show you how to:

*  customize CDE's Workspace Menu
*  place applications icons on the desktop
*  integrate arbitrary window managers and desktop systems with dtlogin


## Customizing CDE's Workspace Menu

CDE's Front Panel is handy for the most important tasks but is the wrong place to put your applications into because it quickly gets jam-packed with lots of icons.  Instead use the Workspace Menu which pops up when clicking the right mouse button on the desktop.  It can be customized quite easily but requires some initial efforts and some understanding of the dt directory structure.

Solaris' desktop configuration is located in `/usr/dt`.  We __never__ do customizations to any of the files contained therein, instead we make copies of single files or complete directory structures and modify them.

This approach is needed to prevent the loss of your customizations resulting from patches and system updates.  System-wide modifications visible to all users of this particular system are put in `/etc/dt` and per-user customizations are expected to be found in `$HOME/.dt`.

The workspace manager searches  for  one  of  the  following resource  description files, where `$LANG` is the value of the language environment on a per-user basis:


* `$HOME/.dt/$LANG/dtwmrc`
* `$HOME/.dt/dtwmrc`
* `/etc/dt/config/$LANG/sys.dtwmrc`
* `/etc/dt/config/sys.dtwmrc`
* `/usr/dt/config/$LANG/sys.dtwmrc`
* `/usr/dt/config/sys.dtwmrc`

The first file found is the first used. If no file is found, a set  of  built-in  specifications  is used.  We can specify exactly three kinds of resources in `dtwmrc`: Buttons, Menus and Keys. In this article I will explain how to create Menu resources only.


### Modifying the Workspace Menu


The factory default of the Workspace Menu is outlined in `/usr/dt/config/sys.dtwmrc` which we copy to `$HOME/.dt/$LANG/dtwmrc`.  The variable `$LANG` denotes the lanugage environment you're currently using which can be any of the names found in `/usr/dt/config`.

If you for example run CDE with en_US.ISO-8859-1 as you're language environment you'll have to copy `sys.dtwmrc` to `$HOME/.dt/en_US.ISO-8859-1/dtwmrc` and modify this file.  (NOTE: It may be an anomaly on my system, but placing `dtwmrc` in `$HOME/.dt/dtwmrc` does not work for me; only files placed in my language environment folder are recognized.)

Usually you don't have to keep all the entries in dtwmrc since it is merged with the files found in `/etc/dt` and `/usr/dt`.  For those not familiar with the syntax, here are some hints on what the entries in `dtwmrc` mean:

```
Menu menu_name {
    label  [mnemonic]  [accelerator]   function
    label  [mnemonic]  [accelerator]   function
              .
              .
    label  [mnemonic]  [accelerator]   function
}
```

Menu is a keyword that denotes the beginning of a menu specification.  It is followed the name of the menu which may contain alphanumeric characters only.  Inside the curly braces are placed the respective menu entries with the mnemonic and the accelerator being optional.

Mnemonics are functional only when the menu is posted and keyboard traversal applies.  Depending on the funtion different or no arguments are expected to follow the function name.

A sample menu specification might look like this:

```
Menu MyMenu {
    "My Menu"               f.title
    "My first Menu Entry"   f.exec "/usr/bin/myapp"
     no-label               f.separator
    "Another Menu Entry"    f.action DtFile
}
```


While `f.exec` expects a string to an executable or script including any arguments, `f.action` invokes an action from the action database. Refer to the `dtwmrc` manpage to see what functions are allowed for which resources and contexts.

Besides `f.exec` and `f.action` other usefull functions are `f.separator` to insert a horizontal line between menu entries and `f.title` to provide a name for the Menu.  `no-label` is a placeholder that effectively expands to an empty string which prevents breaking with the menu specification syntax.

Finally, we'll have to tell the root menu that there is another submenu to be added.  Locate the Menu `RootMenu` entry (which is most likely to be found at the beginning of the file) and add the line:

```
    "My Menu"  f.menu MyMenu
```

The root menu follows the same conventions as other menu specifications.  _My Menu_ is the label which is displayed in the Workspace Menu, `f.menu` is the function that tells the root menu to integrate the menu specification `MyMenu`.  That's it.

When finished with the modifications, you may have to click the "Refresh Workspace Menu" item from the "Windows" menu.  On my Solaris box all alternations are visible at once without the need to refresh the menu explicitly.


## Placing Application Icons on the Desktop


Unfortunately, CDE lacks one of the most important features: letting users place icons on the desktop, either application icons or shortcuts to their favorite documents.

Often, the CDE novice wonders _can I place arbitrary icons on my desktop?_ The answer is _yes_! It’s just different.

It is indeed possible to place icons on the desktop _which are associated with an action_.  As a matter of fact, actions can be augmented with user-defined commands utilizing the `COMMAND` property thus enabling users to put an icon on the desktop which points to a document or URL and is actually represented by an action invoking the appropriate application.

Remember: you can place icons on the desktop, but not arbitrary icons as they _must_ be associated with an action.

To create a desktop icon we will have to create a _Known Application_ a.k.a. _Action_.  Once an action has been created it can be placed on the desktop, invoked from the command line or put into the front panel.

Following is an instruction on how to create an action and how to place it as an icon on the desktop and the front panel.


### Creating an Action


1. Start the _Application Manager_ (click on the icon of a filing cabinet with a pencil).
2. In the _Application Manger_ window, double-click on the Desktop Applications icon.
3. In the _Desktop Applications_ window, double-click on the _Create Action_ icon.
4. Fill in the _Create Action_ panel:

If you chose not to use the application icon set, ignore the following four steps and proceed to step 5  _Command When Action Is Opened_:

1. In the _Action Icons_ area, click on _Find Set_.
1. In the left pane, double-click on _cde$home_defaults:[icons]_
1. In the right pane, click on the application icon.
1. Click on _OK_.
1. In the _Command When Action Is Opened_ field, enter the DCL command starting the application, adding evtl. parameters of the form `%Arg_1%`, like `gv %Arg_1%`.
1. In the _Help Text For Action Icon_ field, enter some help text you want to appear when help is requested.
1. In the _Window Type_ field, select _Graphical (X-Window)_.

If you want to see files with a application icon in the _File Manager_, perform the following steps. This step is optional; you can skip this section and proceed to step 6.

1. Click on the _Advanced_ button on the lower left side of the _Create Action_ window.
2. Click on the _Add_ button next to the _Datatypes That Use This Action_ pane. A new _Add Datatype_ window will open.
3. Click on the _Edit_ button. A new window will open.
4. Click on the small button to the left of the _Name Pattern_ box.
5. Fill in `*.type` in the _Name Pattern_ box.
6. Click on _OK_. You are back in the _Add Datatype_ window.
7. Click on the _Find Set_ button.
8. Choose the icon file you would like to use.
9. Click on _OK_.
10. Click on _OK_ in the _Add Datatype_ window.
11. Repeat for all datatypes you would like to associate the application icon with.

If you previously added the application icon to your desktop, and you now want to enable the application icon in the _File Manager_, use this approach:

1. Perform the steps 1, 2 and 3 above.
2. In the _Create Action_ window select _File -> Open_. You should see a application.dt file in the right pane.
3. Select this file and click on _OK_.
4. When you have finished filling in the _Create Action_ panel, select _File -> Save_.
5. Select _File -> Exit_ to exit the _Create Action_ application.
6. Log out and terminate the CDE session, and then log back in again starting a new CDE session. This is required so that CDE can see the changes you made.


### Making the Application Appear as a Desktop Icon or Entry in the Control Panel


The application is now a _Known Application_ to CDE. To make the application appear as a desktop icon or appear as an entry in the _Front Panel_, perform the following steps.

1. Start the File Manager (click on the icon of a filing cabinet with files).
2. If necessary, scroll down the window until you see the application icon.
3. Create a desktop icon, drag the application icon from the _File Manager_ onto the desktop.

Adding the application to the _Front Panel_:

1. Expand the sub-panel to which you want to add the application (for example, Personal Applications) by clicking on the upwards facing triangle above the sub-panel.
2. Drag the application icon from the _File Manager_ onto the _Install_ icon.


## Integration of Other Window Managers


In case the system(s) you're administering server multiple users with different needs, you may want to provide for them different desktop environments such as KDE, GNOME and the like.  It may therefore be useful to allow users to choose their preferred environment from the _dtlogin_ screen.

Although most of these ready-to-run packages already integrate themselves into `dtlogin` I does not hurt to know how to integrate any window manager or desktop system.  Here is how it can be done.

The system wide default files for CDE configuration are located in `/usr/dt/config` and Sun recommends that if any personal customizations are to be done that they be placed under the `/etc/dt/config directory` (which does not exist by default).

Please follow the following steps to add your favorite window manager to `dtlogin`; I'll use _Enlightenment_ as an example (btw: you will need superuser privileges to make the needed changes):

1. Create the necessary directories:
    `$ mkdir -p /etc/dt/appconfig/icons/C`
    `$ mkdir -p /etc/dt/config/C/Xresources.d`

1. Copy some existing files for modification:
    `$ cp -p /usr/dt/config/Xsession.ow /etc/dt/config/Xsession.el`
    `$ cp -p /usr/dt/config/Xsession.ow2 /etc/dt/config/Xsession.el2`

The new files `Xsession.el` and `Xsession.el2` need to be edited. Basically, change all occurences of `Xsession.ow` to `Xsession.el` and `OpenWindows` to `Enlightenment`. In addition alter any explicit references to `/usr/dt` into `/etc/dt`.

> Please note that, conventionally, there are two setup scripts used in CDE login.

By now `/etc/dt/config/Xsession.el` should look something like this:

```bash
#!/bin/ksh

DTDSPMSG=/usr/dt/bin/dtdspmsg

if [ -z "$SESSIONTYPE" ]; then
    export SESSIONTYPE="altDt"
fi

if [ -z "$DTSTARTIMS" ]; then
    export DTSTARTIMS="False"
fi

if [ -z "$SDT_ALT_SESSION" ]; then
    export SDT_ALT_SESSION="/etc/dt/config/Xsession.el2"
fi

if [ -z "$SDT_ALT_HELLO" ];  then
    if [ -x $DTDSPMSG ]; then
       export SDT_ALT_HELLO="/usr/dt/bin/dthello -string
        '`$DTDSPMSG -s 37 /usr/dt/lib/nls/msg/$LANG/dthello.cat 1
        'Starting Enlightenment'`' &"
    else
       export SDT_ALT_HELLO="/usr/dt/bin/dthello -string
       'Starting Enlightenment' &"
    fi
fi

export SDT_NO_DSDM=""

if [ -x /etc/dt/config/Xsession ]; then
    /etc/dt/config/Xsession
else
    /usr/dt/bin/Xsession
fi
```

And `/etc/dt/config/Xsession.el2` should look like this:

```bash
#!/bin/ksh

unset SDT_ALT_SESSION
unset SDT_ALT_HELLO
unset SDT_NO_DSDM

#Find "xinitrc" file by standard precedence rules and start
#the user's Enlightenment Desktop.

DEFAULT_XINITRC="/etc/dt/config/Xinitrc.el"
HOME_XINITRC="$HOME/.xinitrc"
PATH=/usr/dt/bin:$PATH:/usr/openwin/bin

if [ -z "$XINITRC" ]; then
    if [ -f $HOME_XINITRC ]; then
        XINITRC=$HOME_XINITRC
    else
        XINITRC=$DEFAULT_XINITRC
    fi
fi

echo "Enlightenment Desktop Login"

if [ -f $XINITRC ]; then
    echo "using xinitrc file: $XINITRC"
    /bin/ksh $XINITRC
else
    echo "xinitrc file: $XINITRC not found"

    if [ -f $DEFAULT_XINITRC ]; then
        echo "using xinitrc: $DEFAULT_XINITRC"
        /bin/ksh $DEFAULT_XINITRC
    fi
fi
```

Create a resources file:

```bash
$ cp -p /usr/dt/config/C/Xresources.d/Xresources.ow
/etc/dt/config/C/Xresources.d/Xresources.el
```

Its content should resemble the following:

```
Dtlogin*altDtsIncrement:   True
Dtlogin*altDtName:  Enlightenment Desktop
Dtlogin*altDtKey:   /opt/csw/bin/enlightenment
Dtlogin*altDtStart: /usr/openwin/lib/xdm/Xsession.el
Dtlogin*altDtLogo:  Dtlogo
```

The meaning of the resource names are as follows:

__Dtlogin*altDtsIncrement__
The  presence  of  the altDtsIncrement resource in these files causes an incremented count number to be appended to the resources.  This feature is mostly useful for "package added" desktops.

__Dtlogin*altDtName__
This is the name of the new desktop which will appear in the _Sessions_ menu.

__Dtlogin*altDtKey__
This is what window manager to run.  We specify the executable or script to invoke but do not qualify it.

__Dtlogin*altDtStart__
This is the configuration script.

__Dtlogin*altDtLogo__
This is the logo that appears in the dtlogin window and is a 237x237 XPM file located in `/usr/dt/appconfig/icons/C/`.

NOTE: These files have an extension of `.pm`  not `.xpm`.

Use the correct location for `/opt/csw/bin/enlightenment` on your system or whatever window manager you plan to use.

If you have a suitably sized icon for your window manager you can place a copy in `/etc/dt/appconfig/icons/C` and modify the resource Dtlogin*altDtLogo appropriately. Just specify the name of the image file without extension.  This is a 237 x 237 pixel XPM format file (preferably 256 colors, otherwise it will not work on 8-bit or pseudocolor displays).

Put this file in `/usr/dt/appconfig/icons/C`. Rename it with the extension `.pm` but do not include this extension in the resource string shown above.  For example, if you name your logo file `Ellogo.pm`, simply enter `Ellogo` in `Xresources.el`.
