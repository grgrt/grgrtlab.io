---
title: WSPBuilder's Preflight Check Failed
layout: post
date: 2012-02-15
description: "Deploying a SharePoint solution on a Windows Server 2008 using WSPBuilder may fail throwing some cryptic errors.  This post provides a solution."
---

Deploying a SharePoint solution on a Windows Server 2008 using WSPBuilder may fail with on or more of the following messages:

* SharePoint is not installed on [machine]
* Microsoft Office Server is not installed on [machine]
* Microsoft SharePoint Services Administration is not running on [machine]
* Microsoft SharePoint Services Timer is not running on [machine]

The Internet is full of suggestions how to solve the issue:

* Check in `services.msc` if the services are running. Try to restart them.
* Start Visual Studio with administrative privileges.
* Disable _User Account Control_ completely on [machine]

None worked for me.


## Background Information

So I did what every good developer would do... I fired up Reflector to see what's going on. Apparently, in type `Keutmann.SharePoint.WSPBuilder.Library.Installer` exists the Boolean property `SPTimerRunning` which determines if the required services are up and running. It does so by requesting the current SharePoint version from the registry:

![Registry key VERSION is missing](/assets/img/posts/2012-02-15-01.png)

In my case the `Version` key did not exist for some weird reason which resulted in an empty string to be returned. Thus, the preflight check failed.


## Resolution

To get rid of the errors see if the subkey exists and add a `REG_SZ` it if necessary at:

```
HKEY_LOCAL_MACHINESOFTWAREMicrosoftShared ToolsWeb Server Extensions14.0
```

Enter the correct version number by inspecting the _Servers in Farm_ page in Central Administration. The _Configuration database version_ differs from what is expected to be in the registry. I.e. my system returns 14.0.6029.1000 but the value for the `Version` subkey translates to 14.0.0.6029.

Thanks for reading.
