---
title: Running GitLab 7.9 on Ubuntu 14.04
layout: post
date: 2015-04-15
description: A no-nonsense recipe to your on-premise GitLab instance on Ubuntu.
---


Last month I took a shot at an on-premise deployment of GitLab.  The objective was to understand the technicalities of the process and to learn about the benefits, risks and challenges involved in maintaining the instance.  I took the liberty and documented each and every step we made on our path to enlightenment.  Maybe this script (*script* as in screen-writing, not automation) it is useful to anyone considering to do the same.

Before you start: the script reads more like an NFL playbook than a guide in terms of educational content.  Calling it a guide would be misleading. Having said that, the recipe is geared toward intermediate and advanced users.  If you experience any difficulties I am happy to answer questions and lend a hand if one is needed.

![May your enemies shiver in fear.  Qaplah!](/assets/img/klingon-proverb-001.png)

The whole process requires about 1.5 hours of your time.

***

* TOC
{:toc}


## Part 1: Installing GitLab CE

Part 1 assumes that an Ubuntu/Linux/UNIX-like server is available to host GitLab on.

[GitLab.com](https://about.gitlab.com/downloads/) povides an all-in-one archive for GitLab CE Server, which is perfect to get familiar with most of the functionality.  A very significant drawback is the enormous effort required to integrate with GitLab CI, a complete continuous integration and delivery package.


### Recommendations

1. First, try the manual installation outlined in this document, it actually works well is flexible and simplifies scaling out .
2. Use PostgreSQL as database backend instead of MySQL as stated on GitLab.com.  The former is supposed to be far better suited than MySQL.


### Acquire Base Packages

```sh
# run as root!
apt-get update -y
apt-get upgrade -y
apt-get install sudo -y

# Install vim and set as default editor
sudo apt-get install -y vim
sudo update-alternatives --set editor /usr/bin/vim.basic

# required packages
sudo apt-get install -y build-essential zlib1g-dev libyaml-dev libssl-dev libgdbm-dev libreadline-dev libncurses5-dev libffi-dev curl openssh-server redis-server checkinstall libxml2-dev libxslt-dev libcurl4-openssl-dev libicu-dev logrotate python-docutils pkg-config cmake libkrb5-dev nodejs

# Install Git
sudo apt-get install -y git-core

# Make sure Git is version 1.7.10 or higher, for example 1.7.12 or 2.0.0
git --version

# if system packaged version is too old
sudo apt-get remove git-core
sudo apt-get install -y libcurl4-openssl-dev libexpat1-dev gettext libz-dev libssl-dev build-essential
cd /tmp
curl -L --progress https://www.kernel.org/pub/software/scm/git/git-2.1.2.tar.gz | tar xz
cd git-2.1.2/
./configure
make prefix=/usr/local all
sudo make prefix=/usr/local install
```

In the next to last step of this section you will create a symbolic link to this git binary at `/usr/local/bin/git` making it much easier to work with.  For now we can ignore the defaults.


```sh
# do not use exim4 with Debian and GitLab; select 'Internet Site', confirm hostname
sudo apt-get install -y postfix
```

***

### Install: Ruby...

```sh
# remove old Ruby and install newer version
sudo apt-get remove ruby1.8
mkdir /tmp/ruby && cd /tmp/ruby
curl -L --progress http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.5.tar.gz | tar xz
cd ruby-2.1.5
./configure --disable-install-rdoc
make
sudo make install

# install the Bundler gem
sudo gem install bundler --no-ri --no-rdoc
```

***

### ...System Users

```sh
# create git user for GitLab operation
sudo adduser --disabled-login --gecos 'GitLab' git
```

***

### ...Database (Postgresql)

```sh
# install the database packages
sudo apt-get install -y postgresql postgresql-client libpq-dev

# login to Postgresql, create a user and database for GitLab
sudo -u postgres psql -d template1
template1=# CREATE USER git CREATEDB;
template1=# CREATE DATABASE gitlab_dev OWNER git;

# Quit the database session
template1=# \q

# Try connecting to the new database with the new user
sudo -u git -H psql -d gitlab_dev

# Quit the database session
gitlab_dev> \q
```

***

### ...Redis

```sh
sudo apt-get install redis-server

# Configure redis to use sockets
sudo cp /etc/redis/redis.conf /etc/redis/redis.conf.orig

# Disable Redis listening on TCP by setting 'port' to 0
sed 's/^port .*/port 0/' /etc/redis/redis.conf.orig | sudo tee /etc/redis/redis.conf

# Enable Redis socket for default Debian / Ubuntu path
echo 'unixsocket /var/run/redis/redis.sock' | sudo tee -a /etc/redis/redis.conf

# Grant permission to the socket to all members of the redis group
echo 'unixsocketperm 770' | sudo tee -a /etc/redis/redis.conf

# Create the directory which contains the socket
mkdir /var/run/redis
chown redis:redis /var/run/redis
chmod 755 /var/run/redis

# Persist the directory which contains the socket, if applicable
if [ -d /etc/tmpfiles.d ]; then
  echo 'd  /var/run/redis  0755  redis  redis  10d  -' | sudo tee -a /etc/tmpfiles.d/redis.conf
fi

# Activate the changes to redis.conf
sudo service redis-server restart

# Add git to the redis group
sudo usermod -aG redis git
```

***

### ...GitLab

```sh
# We'll install GitLab into home directory of the user "git"
cd /home/git

# Clone GitLab repository
sudo -u git -H git clone https://gitlab.com/gitlab-org/gitlab-ce.git -b 7-9-stable gitlab

# Go to GitLab installation folder
cd /home/git/gitlab

# Copy the example GitLab config
sudo -u git -H cp config/gitlab.yml.example config/gitlab.yml

# Make sure GitLab can write to the log/ and tmp/ directories
sudo chown -R git log/
sudo chown -R git tmp/
sudo chmod -R u+rwX,go-w log/
sudo chmod -R u+rwX tmp/

# Create directory for satellites
sudo -u git -H mkdir /home/git/gitlab-satellites
sudo chmod u+rwx,g=rx,o-rwx /home/git/gitlab-satellites

# Make sure GitLab can write to the tmp/pids/ and tmp/sockets/ directories
sudo chmod -R u+rwX tmp/pids/
sudo chmod -R u+rwX tmp/sockets/

# Make sure GitLab can write to the public/uploads/ directory
sudo chmod -R u+rwX  public/uploads

# Copy the example Unicorn config
sudo -u git -H cp config/unicorn.rb.example config/unicorn.rb

# Find number of cores
nproc

# Enable cluster mode if you expect to have a high load instance
# Ex. change amount of workers to 3 for 2GB RAM server
# Set the number of workers to at least the number of cores
sudo -u git -H editor config/unicorn.rb

# Copy the example Rack attack config
sudo -u git -H cp config/initializers/rack_attack.rb.example config/initializers/rack_attack.rb

# Configure Git global settings for git user, useful when editing via web
# Edit user.email according to what is set in gitlab.yml
sudo -u git -H git config --global user.name "GitLab"
sudo -u git -H git config --global user.email "mail@gitlab.local"
sudo -u git -H git config --global core.autocrlf input

# Configure Redis connection settings
sudo -u git -H cp config/resque.yml.example config/resque.yml

# Change the Redis socket path if you are not using the default Debian / Ubuntu configuration
sudo -u git -H editor config/resque.yml
```

***

In this step you will have to make sure to edit both `gitlab.tml` and `unicorn.rb` to match your setup.

***

```sh
# link with Postgresql
sudo -u git cp config/database.yml.postgresql config/database.yml

# MySQL only:
sudo -u git cp config/database.yml.mysql config/database.yml

# MySQL and remote Postgresql only:
# Change 'secure password' with the value you have given to $password
sudo -u git -H editor config/database.yml

# Postgresql and MySQL:
# Make config/database.yml readable to git only
sudo -u git -H chmod o-rwx config/database.yml

# install gems for Postgreql (note: without 'mysql')
sudo -u git -H bundle install --deployment --without development test mysql aws

# Run the installation task for gitlab-shell (replace `REDIS_URL` if needed):
sudo -u git -H bundle exec rake gitlab:shell:install[v2.6.0] REDIS_URL=unix:/var/run/redis/redis.sock RAILS_ENV=production

# By default, the gitlab-shell config is generated from your main GitLab config.
# You can review (and modify) the gitlab-shell config as follows:
sudo -u git -H editor /home/git/gitlab-shell/config.yml

# initialize database, activate advanced features, type 'yes', wait a second
sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production
```

***

You set the root password by assining environment variable `GITLAB_ROOT_PASSWORD` a value of your choice as shown below.  If you don't set the password (thus using the default one) **you must not route this instance outside your organization**.

***

```sh
sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production GITLAB_ROOT_PASSWORD=yourpassword

# install init script
sudo cp lib/support/init.d/gitlab /etc/init.d/gitlab
sudo cp lib/support/init.d/gitlab.default.example /etc/default/gitlab
```

***

If you installed GitLab in another directory or as a user other than the default you should change these settings in `/etc/default/gitlab`.  Do not edit `/etc/init.d/gitlab` as it will be overwritten on by patches and updates.

***

```sh
# make GitLab start on boot:
sudo update-rc.d gitlab defaults 21

# setup Logrotate
sudo cp lib/support/logrotate/gitlab /etc/logrotate.d/gitlab

# check if GitLab and its environment are configured correctly:
sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production

# compile Assets
sudo -u git -H bundle exec rake assets:precompile RAILS_ENV=production
```
***

### Nginx
***

```sh
# installation
sudo apt-get install -y nginx

# site config: copy the example site config:
sudo cp lib/support/nginx/gitlab /etc/nginx/sites-available/gitlab
sudo ln -s /etc/nginx/sites-available/gitlab /etc/nginx/sites-enabled/gitlab

# Change YOUR_SERVER_FQDN to the fully-qualified domain name of the GitLab host.
# and remobe both 'listen' rules.
sudo editor /etc/nginx/sites-available/gitlab
```


If you want to use HTTPS, replace the *gitlab* Nginx config with *gitlab-ssl*.

```sh
# validate 'gitlab' or 'gitlab-ssl' Nginx config file
sudo nginx -t

# restart Nginx
sudo service nginx restart

# to make sure you didn't miss anything run a more thorough check:
sudo -u git -H bundle exec rake gitlab:check RAILS_ENV=production

# start / stop GitLab
sudo service gitlab start
sudo service gitlab stop
```

***

### Default Account

At the time of this writing the the following account info was used to login the GitLab instance:

- URL: `https://gitlab.local`
- User: `root`
- Password: `root:5iveL!fe`

This concludes part 1.  If you take no interest in continuos integration with GitLab CI your journey may now begin.  Otherwise feel free to move on to part 2 at your convenience.


***

## Part 2: Installing GitLab CI

If you have been cooking up this recipe to this point,  your are almost done. Compared to part 1 deploying the CI component will come relatively easy to you.


### Packages / Dependecies

Reference: `http://gitlabci.local` (127.0.0.1:8181))

```sh
# run as root!
apt-get update -y
apt-get upgrade -y
apt-get install sudo -y

# Install vim and set as default editor
sudo apt-get install -y vim
sudo update-alternatives --set editor /usr/bin/vim.basic

# install the required packages:
sudo apt-get install wget curl gcc checkinstall libxml2-dev libxslt-dev \
	libcurl4-openssl-dev libreadline6-dev libc6-dev libssl-dev libmysql++-dev \
	make build-essential zlib1g-dev openssh-server git-core libyaml-dev \
	postfix libpq-dev libicu-dev openssl nodejs
sudo apt-get install redis-server
```

***

### Ruby (only required if CI server is hosted on a separate machine)

```sh
# download Ruby and compile it:
mkdir /tmp/ruby && cd /tmp/ruby
curl --progress http://cache.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p353.tar.bz2 | tar xj
cd ruby-2.0.0-p353
./configure --disable-install-rdoc
make
sudo make install

# install the Bundler Gem:
sudo gem install bundler --no-ri --no-rdoc
```

***

### GitLab CI user

```sh
# service account for CI
sudo adduser --disabled-login --gecos 'GitLab CI' gitlab_ci
```

***

### Database

```sh
# Postgresql
sudo apt-get install -y postgresql postgresql-client libpq-dev
sudo -u postgres psql -d template1
template1=# CREATE USER gitlab_ci;
template1=# CREATE DATABASE gitlab_ci_production OWNER gitlab_ci;
template1=# \q

# try to connect with new user
sudo -u gitlab_ci -H psql -d gitlab_ci_production
```

***

### Get code

```sh
cd /home/gitlab_ci/
sudo -u gitlab_ci -H git clone https://gitlab.com/gitlab-org/gitlab-ci.git
cd gitlab-ci
sudo -u gitlab_ci -H git checkout 7-9-stable
```

***

### Setup Application

```sh
# Edit application settings
# Production
sudo -u gitlab_ci -H cp config/application.yml.example config/application.yml
sudo -u gitlab_ci -H editor config/application.yml

# Edit web server settings
sudo -u gitlab_ci -H cp config/unicorn.rb.example config/unicorn.rb
sudo -u gitlab_ci -H editor config/unicorn.rb

# Create socket and pid directories
sudo -u gitlab_ci -H mkdir -p tmp/sockets/
sudo chmod -R u+rwX  tmp/sockets/
sudo -u gitlab_ci -H mkdir -p tmp/pids/
sudo chmod -R u+rwX  tmp/pids/

# install gems for Postgresql (note, the option says "without ... mysql")
sudo -u gitlab_ci -H bundle install --without development test mysql --deployment

# setup postgresql DB
sudo -u gitlab_ci -H cp config/database.yml.postgresql config/database.yml

# Edit user/password (not necessary with default Postgres setup)
sudo -u gitlab_ci -H editor config/database.yml

# Setup tables and schedules
sudo -u gitlab_ci -H bundle exec rake setup RAILS_ENV=production
sudo -u gitlab_ci -H bundle exec whenever -w RAILS_ENV=production

# install Init Script, copy it (will be /etc/init.d/gitlab_ci):
sudo cp /home/gitlab_ci/gitlab-ci/lib/support/init.d/gitlab_ci /etc/init.d/gitlab_ci

# Make GitLab CI start on boot:
sudo update-rc.d gitlab_ci defaults 21

# Start your GitLab CI instance:
sudo service gitlab_ci start

# or
sudo /etc/init.d/gitlab_ci start
```

***

### Nginx

```sh
sudo apt-get install nginx

# Site configuration: download an example site config:
sudo cp /home/gitlab_ci/gitlab-ci/lib/support/nginx/gitlab_ci /etc/nginx/sites-available/gitlab_ci
sudo ln -s /etc/nginx/sites-available/gitlab_ci /etc/nginx/sites-enabled/gitlab_ci

# edit config file to match your setup: Change **YOUR_SERVER_IP** and **YOUR_SERVER_FQDN**
# to the IP address and fully-qualified domain name of your host serving GitLab CI
sudo editor /etc/nginx/sites-enabled/gitlab_ci

# check config and start Nginx
sudo nginx -t
sudo /etc/init.d/nginx start
```


DONE!
