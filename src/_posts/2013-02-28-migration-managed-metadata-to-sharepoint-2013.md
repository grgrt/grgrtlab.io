---
title: Migrating Managed Metadata to SharePoint 2013
layout: post
date: 2013-02-28T00:00:00Z
description: Upgrading from SharePoint 2010 to SharePoint 2013 involves more than just detaching and attaching content DBs. Following the official documentation may get you nowhere, though.  This short post describes a simple, yet effective, approach.
---

{% include message.html text='Upgrading from SharePoint 2010 to SharePoint 2013 involves more than just detaching and attaching content DBs. Following the official documentation may get you nowhere, though.  This short post describes a simple, yet effective, approach.'%}

Today, let's upgrade a _Managed Metadata_ service application from SP 2010 to SharePoint 2013.

Following the [official documentation](http://technet.microsoft.com/en-us/library/jj839719.aspx) on the topic did not lead to any positive results. In case you consider doing the same thing... don't bother, I found very simple way to do the same thing. Taking the TechNet approach will result in an inaccessible Term Store:

{% include message.html mood='info' text='The Managed Metadata service or connection is currently not available. The Application Pool or Managed Metadata Web Service may not have been started. Please contact your administrator.'%}

I've gone through everything: checked application pool accounts, checked if service account has proper permissions to access the database, checked service associations, etc. Nothing helped. The solution, though, was super simple.


## The Database

SharePoint will perform a schema upgrade on the database when attaching it the first time in a SharePoint farm. Before we do that, we will have to perform a backup of the Managed Metadata service application database and perform a restore on the new SQL Server used for your new SharePoint 2013 farm:


### Use SSMS or T-SQL to perform the backup:

```sql
USE SP_D_F1_Metadata;
GO
BACKUP DATABASE SP_D_F1_Metadata TO DISK = 'D:/Data/Backup/SP_D_F1_Metadata.bak'
	WITH FORMAT, MEDIANAME = 'MyBackups', NAME = 'Backup of SP_D_F1_Metadata';
GO
```

### Restore DB on the target server:

```sql
RESTORE DATABASE SP_D_F1_Metadata
	FROM DISK = '//srv01/SP_D_F1_Metadata.bak' WITH REPLACE, RECOVERY;
GO
```

Done.  In theory at least.


## The Migration: TechNet Approach (failed)

Now comes the funny part. According to [TechNet](http://technet.microsoft.com/en-us/library/jj839719.aspx) (same link as above), the following sequence of PowerShell commands invoked on the new SharePoint 2013 farm, of course, will result in a functioning Managed Metadata service application:

```powershell
$applicationPool = Get-SPServiceApplicationPool
  -Identity 'SharePoint Web Services System'

$mms = New-SPMetadataServiceApplication
  -Name 'Managed Metadata Service Application'
	-ApplicationPool $applicationPool
	-DatabaseName 'SP_D_F1_MetaData'

New-SPMetadataServiceApplicationProxy
  -Name 'Managed Metadata Service Application Proxy'
	-ServiceApplication $mms
	-DefaultProxyGroup</pre>
```

The second line will tell SharePoint to use an existing (here: our restored-from-2010-db) instead of creating a new one. Just make sure you pass it the name of the DB you restored earlier. As a result, the service application was there but inaccessible.


## The Migration: Common Sense Approach (worked)

All I had to do was to tell the existing (or newly created) Managed Metadata application to use the restored database instead of its new one.

1.  Just to be on the safe side: delete the existing Managed Metadata service application.
2.  Create a new one using the wizard in Central Administration. Be sure to use the correct application pool or create a new one.
3.  Check the flag _Add this service application to the farm's default list._ (enabled by default)
4.  Provide a dummy name for the database. We are going to replace it anyway, shortly.
5.  Click _OK_ and wait for the new service application to be created.

Force the Managed Metadata service application to use the restored database from the _The Database_ section:

1.  Visit the _Manage service applications page_ in Central Administration.
2.  Select the _Managed Metadata_ service application.
3.  Click _Administrators_ in the ribbon and assign administrative users. This step is necessary. You will receive a permission denied error message if the current user has no administrative privileges
4.  Click _Properties_ in the ribbon.
5.  Change the field _Database Name_ to the one we have restored. This can be done with a single line of PowerShell:

```powershell
Set-SPMetadataServiceApplication
  -Database 'SP_D_F1_MetaData'
```

1.  Click _OK_.
2.  Click _Manage_ in the ribbon to see if the application is available.

Done.

Thanks for reading.
