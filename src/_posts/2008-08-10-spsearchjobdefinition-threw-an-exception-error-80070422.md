---
layout: post
title: SPSearchJobDefinition threw an Exception
date: 2008-08-10
description: "An alternative approach to solving the infamous error 80070422."
---

If you're running a MOSS 2007 deployment you probably came across error 80070422 (or one of its incarnations) in the Windows event log.
{: .post-intro}

This might look familiar:

{% include media-image.html basename="2008-08-10-figure1" extension="png" linkToOriginal=true alt="Figure 1" title="Corresponding entry in the Windows event log" %}

As far as I can tell there are two ways to solve the issue: a hard way and an easy way.


## The Hard Way

One of our customers has resolved the issue with the help of Microsoft's support team:

1.  Create a new Shared Service Provider including a new web application.
2.  Associate your web applications with the new SSP.
3.  Initiate a full crawl for the newly created SSP.

Sounds like a lot of work to me and I believe I came up with a simpler approach.


## The Easy Way

When Office SharePoint Server Search (OSearch) is configured for the first time the Windows SharePoint Services Search service is automatically set to Startup Type _disabled_. Because OSearch takes over from now on. To get rid of the error message follow these 3 simple steps:

1.  Change the _Windows SharePoint Services Search service'_ startup type from _Disabled_ to _Manual_.
2.  In the Central Administration navigate to _Operations > Services_ on Server and stop the _Windows SharePoint Services Help Search_ service and start it again.
3.  Take a look at the _Database Name_ in the _Search Database_ Section and purge/delete the database it using the Microsoft SQL Server Management Studio. Don't worry, it does not hurt.
4.  Re-enter the credentials and press OK to start the service.

From now on I never saw this error again and I did not have to associate all web applications to a new SSP.
