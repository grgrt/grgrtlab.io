---
layout: page
title: About
---

This is a cheap knock-off of [Lanyon](http://lanyon.getpoole.com) _the unassuming [Jekyll](http://jekyllrb.com) theme that places content first by tucking away navigation in a hidden drawer_ [cit. [@mdo](https://twitter.com/mdo)] which itself is based on the [Poole](http://getpoole.com) theme.


## Original ‘Lanyon’ Theme

Mark Otto's Lanyon theme is probably as popular as his work on TWBS:

{% include media-image.html basename="1lanyon" extension="png" linkToOriginal=true alt="Lanyon" title="Original Lanyon theme" %}


## GRGRT Theme

Technically, it is still Lanyon.  The most significant change has been made to the fonts used throughout the site:

{% include media-image.html basename="2grgrt" extension="png" linkToOriginal=true alt="GRGRT" title="Lanyon-based GRGRT (this) theme" %}


## Contents

- [Usage](#usage)
- [Modifications](#modifications)
- [Options](#options)
  - [Sidebar menu](#sidebar-menu)
  - [Themes](#themes)
  - [Reverse layout](#reverse-layout)
- [Development](#development)
- [Author](#author)
- [License](#license)


## Usage

Following the KISS principle, the site is built on top of *Lanyon* providing an approachable Jekyll setup — just download or clone the repo and start the Jekyll server.


### Install dependencies

Before getting started, you'll need to install the [Jekyll](https://jekyllrb.com) gem in case you have not used it before:

```sh
gem install jekyll
```

Windows users might find [@juthilo](https://github.com/juthilo)’s tutorial [Run Jekyll on Windows guide](https://github.com/juthilo/run-jekyll-on-windows) helpful.


### Clone the repository or download an archive

The following command will clone the repository to a directory called `my-site`:

```sh
git clone https://gitlab.com/graegerts/graegerts.gitlab.io.git my-site
```


### Build the site

Before you can take a look at your new site you will have to make sure that all plugins are available before building:

```sh
bundle   # shortcut for bundle install
bundle exec jekyll build
```

See [the Poole usage guidelines](https://github.com/poole/poole#usage) for how to install and use Jekyll.


## Modifications

To accommodate my needs I performed the following subtle modifications:

- __Font Replacements__ to help improve the site’s visual hierarchy.
	- _PT Sans_ with _Helvetica Neue LT_ used for headings, captions and metadata.
	- _PT Serif_ with _Ashbury_ is a screen-friendly serif font
- __Additional Plug-Ins__
	- [jekyll-responsive_image](https://github.com/wildlyinaccurate/jekyll-responsive-image) simplifies handling images in responsive contexts by automatically resizing images according to specified breakpoints.
	- [jekyll-asset_bundler](https://github.com/moshen/jekyll-asset_bundler) minifies CSS and JavaScript.
	- [jekyll-email-protect](https://github.com/vwochnik/jekyll-email-protect) improves spam protection.
	- [jekyll-figure](https://github.com/lmullen/jekyll_figure) provides a shortcut for including figures in multiple formats (SVG, PNG, PDF) with captions.


## Options

Lanyon includes some customizable options, typically applied via classes on the `<body>` element.


### Sidebar menu

Create a list of nav links in the sidebar by assigning each Jekyll page the correct layout in the page's [front-matter](http://jekyllrb.com/docs/frontmatter/).

```
---
layout: page
title: About
---
```

**Why require a specific layout?** Jekyll will return *all* pages, including the `atom.xml`, and with an alphabetical sort order. To ensure the first link is *Home*, we exclude the `index.html` page from this list by specifying the `page` layout.


### Color Themes

Lanyon ships with eight optional themes based on the [base16 color scheme](https://github.com/chriskempson/base16). Apply a theme to change the color scheme (mostly applies to sidebar and links).

![Lanyon with red theme](https://f.cloud.github.com/assets/98681/1825270/be065110-71b0-11e3-9ed8-9b8de753a4af.png)
![Lanyon with red theme and open sidebar](https://f.cloud.github.com/assets/98681/1825269/be05ec20-71b0-11e3-91ea-a9138ef07186.png)

There are eight themes available at this time.

![Available theme classes](https://f.cloud.github.com/assets/98681/1817044/e5b0ec06-6f68-11e3-83d7-acd1942797a1.png)

To use a theme, add any one of the available theme classes to the `<body>` element in the `default.html` layout, like so:

```html
<body class="theme-base-08">
  ...
</body>
```

To create your own theme, look to the Themes section of [included CSS file](https://github.com/poole/lanyon/blob/master/public/css/lanyon.css). Copy any existing theme (they're only a few lines of CSS), rename it, and change the provided colors.


### Reverse layout

![Lanyon with reverse layout](https://f.cloud.github.com/assets/98681/1825265/be03f2e4-71b0-11e3-89f1-360705524495.png)
![Lanyon with reverse layout and open sidebar](https://f.cloud.github.com/assets/98681/1825268/be056174-71b0-11e3-88c8-5055bca4307f.png)

Reverse the page orientation with a single class.

```html
<body class="layout-reverse">
  ...
</body>
```


### Sidebar overlay instead of push

Make the sidebar overlap the viewport content with a single class:

```html
<body class="sidebar-overlay">
  ...
</body>
```

This will keep the content stationary and slide in the sidebar over the side content. It also adds a `box-shadow` based outline to the toggle for contrast against backgrounds, as well as a `box-shadow` on the sidebar for depth.

It's also available for a reversed layout when you add both classes:

```html
<body class="layout-reverse sidebar-overlay">
  ...
</body>
```

### Sidebar open on page load

Show an open sidebar on page load by modifying the `<input>` tag within the `sidebar.html` layout to add the `checked` boolean attribute:

```html
<input type="checkbox" class="sidebar-checkbox" id="sidebar-checkbox" checked>
```

Using Liquid you can also conditionally show the sidebar open on a per-page basis. For example, here's how you could have it open on the homepage only:

```html
<input type="checkbox" class="sidebar-checkbox" id="sidebar-checkbox" {% if page.title =="Home" %}checked{% endif %}>
```


## Author

**Mark Otto** (Lanyon, Poole)\\
<https://github.com/mdo>\\
<https://twitter.com/mdo>

**Steve Graegert** (GRGRT)\\
<https://gitlab.com/graegerts>\\
<http://graegert.com> (Alter Ego)


## License

Open sourced under the [MIT license](LICENSE.md).




Have questions or suggestions? Feel free to [open an issue on GitLab](https://gitlab.com/graegerts/graegerts.gitlab.io/issues) or [ask me via email](mailto:{{ site.email | encode_email }}).

Thanks for reading!
