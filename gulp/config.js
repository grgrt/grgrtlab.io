/*
 *
 *
 *    ____    ____    ____    ____    ______
 *   /\  _`\ /\  _`\ /\  _`\ /\  _`\ /\__  _\
 *   \ \ \L\_\ \ \L\ \ \ \L\_\ \ \L\ \/_/\ \/
 *    \ \ \L_L\ \ ,  /\ \ \L_L\ \ ,  /  \ \ \
 *     \ \ \/, \ \ \\ \\ \ \/, \ \ \\ \  \ \ \
 *      \ \____/\ \_\ \_\ \____/\ \_\ \_\ \ \_\
 *       \/___/  \/_/\/ /\/___/  \/_/\/ /  \/_/ 8-nov-14
 *
 *
 * Configuration file for Gulp.
 * Released under MIT license by @graegert.
 */

var nodes = 'node_modules';                                     // 'node_modules' folder
var src = 'src';                                                // the base folder we are developing in
var srcAssets = 'src/_assets';                                  // parent directory for CSS, JS, img, fonts
var build = 'build';                                            // home PRODuction and DEVelopment builds
var development = 'build/development';                          // DEVelopment build root (Jekyll drops its artifacts here)
var developmentAssets = 'build/assets';                         // DEVelopment assets (not processed by Jekyll)
var production = 'build/production';                            // PRODuction build root (Jekyll drops its artifacts here)
var productionAssets = 'build/production/assets';               // PRODuction assets (not processed by Jekyll)

module.exports = {
    globals: {
      debug: false
    },

    /**
     * BROWSER REFRESH FOR DEV & PROD
     * ------------------------------
     * Tasks:   'default', ['watch', ['browsersync']]
     * Modules: 'gulp', 'browser-sync'
     *
     * BrowserSync watches only asset files to prevent the browser from reloading
     * far to frequently.
     * Both 'watch' and 'browsersync' will run suimultaneously.
     *
     */
    browsersync: {
        development: {
            server: {
                baseDir: [development, build, src]
            },
            port: 9999,
            files: [
                developmentAssets + '/css/*.css',
                developmentAssets + '/js/*.js',
                developmentAssets + '/img/*/**',
                developmentAssets + '/fonts/*/**'
            ]
        },
        production: {
            server: {
                baseDir: [production]
            },
            port: 9998
        }
    },

    /**
     * CLEAN UP, BUILD & JEKYLL
     * ------------------------
     * Tasks:   'jekyll' => 'jekyll-rebuild', ['jekyll']
     * Modules: 'runsequence', 'del', 'child_process'
     *
     * Array of folders to delete before development builds start.
     * Used by Gulp task 'build' to clean up before kicking off.
     */
    delete: {
        src: [build]
    },

    /**
     * JEKYLL BUILD SETTINGS FOR DEV & PROD
     * ------------------------------------
     * Tasks:   'build' => runSequence('delete', ['jekyll', 'sass', 'scripts', 'images', 'copy:fonts' ], 'base64')
     * Modules: 'run-sequence'
     *
     * Both build targets (DEV, PROD) fetch code from 'src' but write their
     * artifacts to separate folders.
     * The Jekyll site build, SASS files compilation, bundling of JavaScript files
     * is done simultaneously, copies images and copy vector fonts to respective
     * assets folders.
     * Once the sass task is finished, 'base64' converts small PNG files to Base64
     * encoding and inlines them in CSS.
     *
     * Do not remove the YAML pair or change its  order; *-dev.yml overwrites
     * sepcific settings of PROD's configuration.
     */
    jekyll: {
        development: {
            src: src,
            dest: development,
            config: '_config.yml,_config-dev.yml'
        },
        production: {
            src: src,
            dest: production,
            config: '_config.yml'
        }
    },

    /**
     * SASS PREPROCESSING, AUTO-PREFIXING
     * ----------------------------------
     * Task:    'sass'
     * Modules: 'gulp-plumber', 'gulp-ruby-sass', 'gulp-filter'
     *
     * - SASS files are compiled by 'gulp-ruby-sass' (with source maps)
     * - applies prefixes for listed vendors
     */
    styles: {
        src: srcAssets + '/css/*.css',
        dest: developmentAssets + '/css',
        options: {
            precss: {},
            autoprefixer: {
                browsers: [
                    'last 2 versions',
                    'safari 5',
                    'ie 8',
                    'ie 9',
                    'opera 12.1',
                    'ios 6',
                    'android 4'
                ],
                cascade: true
            },
            mqpacker: {}
        }
    },

    /**
     * LINTING SCSS/CSS FILES
     * ----------------------
     * Tasks:   'sccslint'
     * Modules: 'gulp-scss-lint', 'gulp-css-lint'
     *
     * The 'src' property is configured with a set of files to check and
     * any exception you want to specify.  Since 'fontcustom' and '_sprite'
     * do not validate properly (and because we do not have ownership) they
     * will explicitly ignored as the exclamation mark ('!') indicates.
     *
     * The 'options' property allows for specification of custom rules to
     * validate the code against.
     *
     * You are encouraged to externalize rulesets in '.scss-lint.yml' (for
     * maintainability) and override selected rules here.
     *
     * @see https://www.npmjs.com/package/gulp-scss-lint and
     * https://github.com/juanfran/gulp-scss-lint for details.
     */
    lintStyles: {
        src: [
            srcAssets + '/css/**/*.css',
            '!' + srcAssets + '/css/partials/_syntax-highlighting.css',
            '!' + srcAssets + '/css/partials/_sprites.css',
            '!' + srcAssets + '/css/partials/fontcustom.css'
        ],
        options: {
            stylelint: {
                'rules': {
                    'string-quotes': [2, 'double'],
                    'color-hex-case': [2, 'lower'],
                    'value-no-vendor-prefix': 2,
                    'declaration-no-important': 0,
                    'rule-non-nested-empty-line-before': [2, 'always', {
                        ignore: ['after-comment']
                    }]
                }
            },
            reporter: {
                clearMessages: true
            }
        }
    },

    /**
     * BUNDLING OF JAVASCRIPT WITH BROWSERIFY
     * --------------------------------------
     * Tasks:   'scripts'
     * Modules: 'browserify', 'vinyl-source-stream', 'watchify', 'browserify-shim'
     *
     * Browserify allows you to run Node modules in browser contexts.
     * Dependencies will be resolved automatically via 'require' as
     * we know from CommonJS module patterns.
     *
     * The projects 'package.json' defines 'browserify-shim' to be handle all
     * all elements specified in its 'browser' section.  This "shim-ing" allows
     * conventional code to function in CommonJS contexts.
     */
    browserify: {
        debug: true,                                        // enable source maps
        extensions: ['.coffee', '.hbs'],                    // additional (yet, optional) file extensions
        bundleConfigs: [{                                   // create an isolated bundle for each entry
            entries: [
                './' + srcAssets + '/js/main.js',
                './' + srcAssets + '/js/scripts-40x.js'
            ],  // 'main.js' contains most of the JavaScript used here
            dest: developmentAssets + '/js',            // drop target for the bundle
            outputName: 'application.js'                // name of the bundle; reference this instead of 'main.js'
        }]
    },

    /**
     * IMAGES & VECTOR FONTS
     * ---------------------
     * Tasks:   'images'
     * Modules: 'gulp-shell'
     *
     * Copies images to asset directories according to build target.
     * Refer to setting 'copyfonts' for drop targets upon compilation.
     */
    images: {
        src: srcAssets + '/img/**/*',
        dest: developmentAssets + '/img'
    },

    /**
     * RESIZE SPECIFIC IMAGES FOR RESPONSIVENESS
     * -----------------------------------------
     * Tasks:   'responsive'
     * Modules: 'gulp-responsive'
     *
     * Creates resized copies of images placed in 'resizable' folder
     * and save them in 'resized' folder.
     */
    responsive: {
        src: srcAssets + '/img/resizable/**/*',
        dest: developmentAssets + '/img/resized',
        options: {

        }
    },

    /**
     * CONVERTING IMAGES TO WEBP
     * -------------------------
     * Tasks:   'webp'
     * Modules: 'gulp-webp'
     *
     * Googles latest innovation "WebP" is an image format for the web with even
     * better compression as JPEG while maintaining extraordinary picture quality.
     *
     * Most contemporary browsers and even those almost unkown to ordinary
     * users (like Konquerer) support WebP.
     *
     * It is worth mentioning that this task is only active for production builds
     * and that we have to rewrite URLs for those images via '.htaccess'.  No
     * worries, the toolchain got you covered and will do it for you.
     *
     * However, the DEV webserver is unable to process WebP files due to its lack of
     * rewriting capabilities in general.  The .htaccess will have no effect when
     * serving your site in DEV mode.
     */
    webp: {
        src: productionAssets + '/img/**/*.{jpg,jpeg,png}',
        dest: productionAssets + '/img/',
        options: {}
    },

    /**
     * COMPRESSING STATIC SITE ELEMENTS
     * --------------------------------
     * Tasks:   'gzip'
     * Modules: 'gulp-gzip'
     *
     * Most contemporary web servers perform some sort of compression to increase
     * bandwidth utilization.  It is considered best practice to compress items anyway
     * because the web server saves enormous amounts of CPU cycles and we usually
     * achieve significantly higher compression rates as web servers rarely perform
     * compression at the highest possible level.
     */
    gzip: {
        src: production + '/**/*.{html,xml,json,css,js}',
        dest: production,
        options: {}
    },

    /**
     * TRANSFER FONTS TO ASSETS FOLDER
     * -------------------------------
     * Tasks:   'copy:fonts'
     * Modules: 'gulp-shell'
     *
     * Job can be run in parallel.
     */
    copyfonts: {
        development: {
            src: srcAssets + '/fonts/**/*',
            dest: developmentAssets + '/fonts'
        },
        production: {
            src: developmentAssets + '/fonts/**/*',
            dest: productionAssets + '/fonts'
        }
    },

  /**
   * TRANSFER VENDOR JS TO ASSETS FOLDER
   * -----------------------------------
   * Tasks:   'copy:vendorjs'
   * Modules: 'gulp-shell'
   *
   * Job can be run in parallel.
   */
  copyvendorjs: {
    development: {
      src: [
        nodes + '/imager.js/dist/Imager.min.js',
        nodes + '/jquery/dist/jquery.min.js'
      ],
      dest: developmentAssets + '/js/vendor'
    },
    production: {
      src: [
        nodes + '/imager.js/dist/Imager.min.js',
        nodes + '/jquery/dist/jquery.min.js'
      ],
      dest: productionAssets + '/js/vendor'
    }
  },

    /**
     * SETTINGS FOR PNG BASE64 ENCODING
     * --------------------------------
     * Tasks:   'base64', ['sass']
     * Modules: 'gulp-base64'
     *
     * Browsers handle small images encoded in Base64 much more efficiently
     * resulting in optimal performance as long as they are not heavier than
     * 20 KB.  Still, YMMV.
     */
    base64: {
        src: developmentAssets + '/css/*.css',
        dest: developmentAssets + '/css',
        options: {
            baseDir: build,
            extensions: ['png'],
            maxImageSize: 20 * 1024, // bytes
            debug: false
        }
    },

    /**
     * WATCHING FOR CHANGES AND ACT UPON THEM
     * --------------------------------------
     * Tasks:   'watch', ['browsersync']'
     * Modules: 'gulp'
     *
     * Observing files and acting upon changes is a capability provided
     * by the 'gulp' module itself.  This config setting is a collection
     * of artifacts we want to watch and trigger actions in case they
     * change.
     *
     * Refer to task 'watch' to understand events and actions associated
     * with watched elements.
     */
    watch: {
        jekyll: [
            '_config.yml',
            '_config.build.yml',
            'stopwords.txt',
            src + '/_data/**/*.{json,yml,csv}',
            src + '/_includes/**/*.{html,xml}',
            src + '/_layouts/*.html',
            src + '/_locales/*.yml',
            src + '/_plugins/*.rb',
            src + '/_posts/*.{markdown,md}',
            src + '/**/*.{html,markdown,md,yml,json,txt,xml}',
            src + '/*'
        ],
        styles: srcAssets + '/css/**/*.css',
        scripts: srcAssets + '/javascripts/**/*.js',
        images: srcAssets + '/img/**/*',
        sprites: srcAssets + '/img/**/*.png',
        svg: 'vectors/*.svg'
    },

    /**
     * LINTING JAVASCRIPT CODE
     * -----------------------
     * Tasks:   'jshint'
     * Modules: 'gulp-jshint', 'jshint-stylish'
     *
     * JSHint will help you identify sequences of code that may not adhere
     * to certain styles or priciples as well as potential risks caused by
     * sloppiness or ambiguity which may erode maintainability or result in
     * technical debt.
     *
     * You are encouraged to externalize rulesets in '.jshintrc' (for
     * maintainability) and override selected rules here.
     *
     * @see https://github.com/spalger/gulp-jshint for details.
     */
    jshint: {
        src: srcAssets + '/js/*.js'
    },

    /**
     * CREATING CSS IMAGE SPRITES
     * --------------------------
     * Tasks:   'sprites'
     * Modules: 'gulp.spritesmith'
     *
     * Image sprites encapsulate multiple images in a single file (like a mosaic)
     * and minimize the number of roundtrips required for clients in order to
     * obtain multiple images.  You can request a segment from a sprite by
     * specifying the segment's coordinates within the sprite.  This rather
     * tedious task is painless thanks clever use of CSS.  Creating image sprites
     * is also easy as pie as you will see in task 'sprites'.
     *
     * The properties 'src' and 'dest' are self-explainatory. 'options' on the other
     * hand is very powerful as illustrated by the function below that creates a
     * suitable hover image when moving the mouse pointer over an image.
     *
     * Upon completion of task 'sprites' you will own an image sprite and a CSS file
     * providing CSS rules (classes) to comfortably address segments from code.
     *
     * @see https://github.com/twolfson/gulp.spritesmith for details
     */
    sprites: {
        src: srcAssets + '/img/sprites/icon/*.png',
        dest: {
            css: srcAssets + '/css/partials/base/',
            image: srcAssets + '/img/sprites/'
        },
        options: {
            cssName: '_sprites.scss',   // use this file to interact with the sprite in code
            cssFormat: 'css',           // the SCSS file will, of course, also be compiled to CSSon the fly
            cssOpts: {
                cssClass: function (item) {
                    // If this is a hover sprite, name it as a hover one (e.g. 'home-hover' -> 'home:hover')
                    if (item.name.indexOf('-hover') !== -1) {
                        return '.icon-' + item.name.replace('-hover', ':hover');
                        // Otherwise, use the name as the selector (e.g. 'home' -> 'home')
                    } else {
                        return '.icon-' + item.name;
                    }
                }
            },
            imgName: 'icon-sprite.png',
            imgPath: '/assets/img/sprites/icon-sprite.png'
        }
    },

    /**
     * OPTIMIZATION OF ARTIFACTS
     * -------------------------
     * Tasks:   'optimize:css', 'optimize:js', 'optimize:images', 'optimize:html'
     * Modules: 'gulp-minify-css', 'gulp-uglify', 'gulp-imagemin', 'gulp-htmlmin', 'gulp-size'
     *
     * Despite faster Internet access and the proliferation of more and more
     * powerfull devices it is your responsibility to reduce the burden on either
     * without compromising the browsing experience for users.
     *
     * Compressing files that are transferred between endpoints provides great
     * taps into great potentials for saving bandwidth and computing power.
     *
     * Compression involves removing all insignificant whitespace from all text
     * files and (optionally) mangling them.  The benefits will vary but it is
     * safe to assume that on average file sizes can be reduced by 60 to 70 percent.
     *
     * Module 'gulp-imagemin' helps reduce the size of images without perceivable
     * consequences.
     */
    optimize: {
        css: {
            src: developmentAssets + '/css/*.css',      // we copy from DEV assets folder to PROD because
            dest: productionAssets + '/css/',           // the files are already there thanks to 'browsersync',
            options: {                                  // 'images' and 'jekyll', of course.
                keepSpecialComments: 0
            }
        },
        js: {
            src: developmentAssets + '/js/*.js',
            dest: productionAssets + '/js/',
            options: {}
        },
        images: {
            src: developmentAssets + '/img/**/*.{jpg,jpeg,png,gif}',
            dest: productionAssets + '/img/',
            options: {
                optimizationLevel: 3,
                progessive: true,
                interlaced: true
            }
        },
        html: {
            src: production + '/**/*.html',
            dest: production,
            options: {
                collapseWhitespace: true
            }
        }
    },

    /**
     * (1/2) FILE REVISIONING AND CACHE BUSTING
     * ----------------------------------------
     * Tasks:   'revision'
     * Modules: 'gulp-rev'
     *
     * Thanks to sophisticated caching huge amounts of extra network traffic
     * and CPU cycles can are making lives easier online as well as offline.
     * Unfortunately, in time caches become "stale" and must be flushed and
     * repopulated, most notably when a cached element has changed.  The
     * question is, though: how can we make sure that caches pick up the
     * changes when pushed to them?  For web developers the solution is simple
     * but extremely inefficient. It is sufficient to rename an item and
     * caches will update its stale predecessor.
     *
     * Revisioning is the process of touching changed elements automatically
     * and rename them in single-handed.  A proven approach is to use an item's
     * MD5 hash and either replace the name or append it to the name.  MD5
     * hashes can be calculated extremely fast even of larger objects and
     * represent an object's "fingerprint".  If, for instance, only one bit of
     * a 5 Megapixel is flipped the newly calculated MD5 hash will change.
     *
     * Since hash values are reasonably unique (globally, statistically speaking)
     * they are perfect for cache busting.
     *
     * Most of the times, however, checksums will get the job done.  Checksums
     * are even faster to calculate and shorter, perfect for our purposes.
     */
    revision: {
        src: {                                          // source
            assets: [
                productionAssets + '/css/*.css',        // specify the items to revision;
                productionAssets + '/js/*.js',          // works with all kinds of file formats;
                productionAssets + '/img/**/*'          // we revision everything except html
            ],
            base: production
        },
        dest: {
            assets: production,                         // destination
            manifest: {
                name: 'manifest.json',                  // created before revisioning, maps filenames to hashes/checksums.
                path: productionAssets
            }
        }
    },

    /**
     * (2/2) FILE REVISIONING AND CACHE BUSTING
     * ----------------------------------------
     * Tasks:   'rev:collect'
     * Modules: 'gulp-rev-collector'
     *
     * Part 1 was concerned with identifying objects to revision and to create
     * the manifest.  Now we have to pick up the manifest and rename all elements
     * that have changed.
     */
    collect: {
        src: [
            productionAssets + '/manifest.json',
            production + '/**/*.{html,xml,txt,json,css,js}',
            '!' + production + '/feed.xml'
        ],
        dest: production
    },

    /**
     * DEPLOYMENT
     * ----------
     * Tasks:   'deploy', ['rsync']
     * Modules: 'gulp-rsync'
     *
     * rsync is used to send and receive (even synchronize) local and remote data sources.
     * It supports a variety of protocols and authentication mechanisms, is Open Source
     * and extremely flexible, making it perfect for automating the deployment process.
     *
     * @see https://www.npmjs.com/package/gulp-rsync for details.
     */
    rsync: {
        src: production + '/**',
        options: {
            destination: '~/path/to/my/website/root/',
            root: production,
            hostname: 'mydomain.com',
            username: 'user',
            incremental: true,
            progress: true,
            relative: true,
            emptyDirectories: true,
            recursive: true,
            clean: true,
            exclude: ['.DS_Store'],
            include: []
        }
    }
};
