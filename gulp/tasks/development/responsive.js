/**
 * Created by graegerts on 29/04/16.
 */

var gulp = require('gulp');
var responsive = require('gulp-responsive');
var config = require('../../config').responsive;

gulp.task('responsive', function () {
    return gulp.src(config.src)
        .pipe(responsive({
            '*': [  // '*' refers to all files in src
                {
                    width: 416,
                    rename: {
                        suffix: '-small'
                    },
                }, {
                    width: 540,
                    rename: {
                        suffix: '-medium'
                    },
                }, {
                    width: 648,
                    rename: {
                        suffix: '-large'
                    },
                }
            ],
        }))
        .pipe(gulp.dest(config.dest));
});
