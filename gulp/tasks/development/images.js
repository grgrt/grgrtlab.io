var gulp        = require('gulp');
var changed     = require('gulp-changed');
var config      = require('../../config').images;

/**
 * Copy only change images only to build folder.
 */
gulp.task('images', ['responsive'], function() {
  return gulp.src(config.src)
    .pipe(changed(config.dest)) // Ignore unchanged files
    .pipe(gulp.dest(config.dest));
});
