var gulp   = require('gulp');
var config = require('../../config').copyvendorjs.development;

/**
 * Copy vendor scripts to folder
 */
gulp.task('copy:vendorjs', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
