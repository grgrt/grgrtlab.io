var gulp   = require('gulp');
var config = require('../../config').copyvendorjs.production;

/**
 * Copy vendor scripts to folder
 */
gulp.task('copy:vendorjs', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
